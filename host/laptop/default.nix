{ pkgs, inputs, ... }:

{
  imports = [
    # add the model from this list: https://github.com/NixOS/nixos-hardware/blob/master/flake.nix
    # TODO Need to create a e550 hardware config and upstream
    inputs.hardware.nixosModules.lenovo-thinkpad-t550
    inputs.hardware.nixosModules.common-pc-ssd

    ../_common/global

    ../_common/optional/hardware-support/laptop-lid.nix
    ../_common/optional/hardware-support/laptop-battery-optimization.nix
    ../_common/optional/hardware-support/hardware-accelerated-video.nix
    ../_common/optional/hardware-support/displaylink.nix
    ../_common/optional/hardware-support/logitech.nix
    ../_common/optional/hardware-support/fingerprint-sensor.nix
    
    ../_common/optional/desktop/dm/gdm.nix
    ../_common/optional/desktop/de/gnome.nix

    ../_common/optional/app/flatpak.nix
    #../_common/optional/app/barrier.nix
    #../_common/optional/app/input-leap.nix
    #../_common/optional/app/syncthing.nix
    #../_common/optional/wifi.nix

    ../_common/users/chris
    ../_common/users/kelsey
    ../_common/users/hannah

     # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  networking.hostName = "laptop"; # Define your hostname.

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}
