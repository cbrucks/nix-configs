{ pkgs, inputs, ... }:

{
  imports = [
    # add the model from this list: https://github.com/NixOS/nixos-hardware/blob/master/flake.nix
    inputs.hardware.nixosModules.common-cpu-intel
    inputs.hardware.nixosModules.common-pc
    inputs.hardware.nixosModules.common-pc-ssd

    ../_common/global

    ../_common/optional/hardware-support/hardware-accelerated-video.nix
    ../_common/optional/hardware-support/logitech.nix
    ../_common/optional/hardware-support/nvidia-graphics-card.nix
    #../_common/optional/hardware-support/desktop-fan-control.nix

    ../_common/optional/desktop/dm/sddm.nix
    ../_common/optional/desktop/de/kde.nix

    ../_common/optional/app/docker-cuda.nix
    # ../_common/optional/app/emulators.nix
    ../_common/optional/app/appimage.nix
    ../_common/optional/app/flatpak.nix
    ../_common/optional/app/openrgb.nix
    ../_common/optional/app/steam.nix
    ../_common/optional/app/sunshine.nix
    # ../_common/optional/app/syncthing.nix
    # ../_common/optional/app/upscayl.nix

    ../_common/users/chris
    ../_common/users/kelsey
    ../_common/users/hannah

     # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  networking.hostName = "desktop"; # Define your hostname.

  hardware.bluetooth = {
    enable = true; # enables support for Bluetooth
    powerOnBoot = true; # powers up the default Bluetooth controller on boot
  };

  hardware.nvidia.prime = {
    offload.enable = false;
    sync.enable = true;
    
    # Bus ID of the Intel GPU. You can find it using lspci;
    # for example if lspci shows the Intel GPU at "00:02.0", set this option to "PCI:0:2:0".
    intelBusId = "PCI:0:2:0";
    # Bus ID of the NVIDIA GPU. You can find it using lspci;
    # for example if lspci shows the NVIDIA GPU at "01:00.0", set this option to "PCI:1:0:0".
    nvidiaBusId = "PCI:1:0:0";
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
