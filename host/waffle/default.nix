{ pkgs, inputs, ... }:

{
  imports = [
    # add the model from this list: https://github.com/NixOS/nixos-hardware/blob/master/flake.nix
    inputs.hardware.nixosModules.common-cpu-intel
    inputs.hardware.nixosModules.common-pc
    inputs.hardware.nixosModules.common-pc-ssd

    ../_common/global

    ../_common/optional/hardware-support/hardware-accelerated-video.nix

    ../_common/optional/desktop/none.nix

    ../_common/optional/app/docker.nix
    #../_common/optional/app/podman.nix

    ../_common/users/chris

     # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  networking.hostName = "waffle"; # Define your hostname.

  # Open firewall for docker web services
  #networking.firewall = {
  #  allowedTCPPorts = [
  #    80   # HTTP
  #    443  # HTTPS
  #    8000 # Portainer edge agent communication
  #    7359 # Jellyfin Network Discovery
  #    1900 # Jellyfin DLNA
  #    2377 # docker swarm
  #  ];
  #};

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
