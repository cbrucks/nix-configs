Lenovo ThinkCentre M700
Machine Type: 10J0
Model: S1XH00
SN: MJ03XYUP

Processor: 6th Generation Intel® Core™ i5-6500T Processor(Core i5-6500T)
Memory: 4GB/8GB DDR4-2133
Hard Drive: 256GBOPAL2.0
Wireless Network: None
Ports:
  - 2 USB 3.0 (front)
  - 4 High Speed USB 3.0 (back)
  - Microphone/Headphone Combo(Nokia and Apple type global headset autodetect)
  - 2 Integrated Display port (Back)
  - Ethernet
  - Audio Line In
  - Headphone Out
  - Expansion Port - VGA
