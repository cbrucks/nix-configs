Dell OptiPlex 7050 Micro
Machine Type: D10U002
Model: D10U
SN: 2Q3N1S2

Processor: 6th Generation Intel® Core™ i5-6600T Processor(Core i5-6600T) (QC/6MB/4T /2.7GHz/35W)
Memory: 
Hard Drive: 128GB
Wireless Network: Intel® Wireless-AC 8260 2x2 AC & Bluetooth® 4.1
Ports:
  - 1 USB-A 3.0 (front)
  - 1 USB-C 3.0 (front)
  - Headphone Out (front)
  - Audio Line-In (front)
  - 4 High Speed USB-A 3.0 (back)
  - 1 Integrated Display port (back)
  - 1 Integrated HDMI port (back)
  - Ethernet (back)
  - Expansion Port - Display Port (back)
Power Requirements: 19.5 VDC 3.34 A