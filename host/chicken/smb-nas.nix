{ config, pkgs, ... }:

{
  # For mount.cifs, required unless domain name resolution is not needed.
  environment.systemPackages = [ pkgs.cifs-utils ];

  fileSystems."/mnt/docker-compose-configs" = {
      device = "//nas.brucks.local/docker";
      fsType = "cifs";
      options = let
        # this line prevents hanging on network split
        automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
        credentials = "credentials=/etc/nixos/secrets/smb-nas-docker";
        permissions = "uid=2000,gid=131,file_mode=0775,dir_mode=0775";
      in ["${automount_opts},${credentials},${permissions}"];
  };

  networking.firewall.extraCommands = ''iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns'';
}

