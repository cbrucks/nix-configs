{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../../hardware-configuration.nix
      ../default.nix
      ../../modules/desktop-environment/gnome.nix
      ../../modules/hardware-support/hardware-accelerated-video.nix
      # ../../modules/displaylink.nix
      # ../../modules/syncthing.nix
      #../../modules/wifi.nix
      ../../users/chris.nix
      ../../users/hannah.nix
    ];

  networking.hostName = "aspire-one"; # Define your hostname.

  # Enable touchpad support (enabled default in most desktopManager).
  # Options: https://search.nixos.org/options?query=services.xserver.libinput
  services.xserver.libinput.enable = true;
  services.xserver.libinput.touchpad.naturalScrolling = true;

  # Good for SSD
  services.fstrim = {
    enable = true;
  };

  # users.groups."syncthing".members = [
  #   "chris"
  # ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}
