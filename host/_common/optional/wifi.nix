{ config, pkgs, ... }:

{
  # example contents of /etc/nix/secrets/wifi-passwords.env
  #   PASS_MONKEYBUTLER=<password1>
  #   PASS_ANTILL=<password2>
  #   PASS_STONECIPHER=<password3>

  # wireless-related configuration
  networking.wireless = {
    enable = true;
    environmentFile = "/etc/nixos/secrets/wifi-passwords.env";
    networks.MonkeyButler.psk = "@PASS_MONKEYBUTLER@";
    networks.Antill.psk = "@PASS_ANTILL@";
    networks.stonecipher.psk = "@PASS_STONECIPHER@";
  };
}