{ config, pkgs, lib, ... }:

{
  services.homepage-dashboard = {
    # Whether to enable Homepage Dashboard, a highly customizable application dashboard.
    enable = true;

    # Homepage docker configuration.
    # See https://gethomepage.dev/latest/configs/docker/.
    docker = {};

    # Homepage widgets configuration.
    # See https://gethomepage.dev/latest/configs/service-widgets/.
    widgets = [];

    # Homepage settings.
    # See https://gethomepage.dev/latest/configs/settings/.
    settings = {};

    # Homepage services configuration.
    # See https://gethomepage.dev/latest/configs/services/.
    services = [];

    # Custom Javascript for Homepage.
    # See https://gethomepage.dev/latest/configs/custom-css-js/.
    customJS = "";

    # Custom CSS for styling Homepage.
    # See https://gethomepage.dev/latest/configs/custom-css-js/.
    customCSS = "";

    # Homepage bookmarks configuration.
    # See https://gethomepage.dev/latest/configs/bookmarks/.
    bookmarks = [];

    # Port for Homepage to bind to.
    listenPort = 8082;

    # Homepage kubernetes configuration.
    # See https://gethomepage.dev/latest/configs/kubernetes/.
    kubernetes = {};

    # Open ports in the firewall for Homepage.
    openFirewall = true;

    # The path to an environment file that contains environment variables to pass to the homepage-dashboard service, for the purpose of passing secrets to the service.
    # See https://gethomepage.dev/latest/installation/docker/#using-environment-secrets
    environmentFile = "";
  };
}