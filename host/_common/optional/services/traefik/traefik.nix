{ config, pkgs, ... }:

{
  services.traefik = {
    enable = true;

    # Set the group that traefik runs under. For the docker backend this needs to be set to docker instead.
    # group = "traefik";
    group = "podman";

    # Location for any persistent data traefik creates, ie. acme
    dataDir = "/var/lib/traefik";

    # Path to traefik’s dynamic configuration to use. (Using that option has precedence over dynamicConfigOptions)
    # dynamicConfigFile = /path/to/dynamic_config.toml;
    # Dynamic configuration for Traefik.
    # dynamicConfigOptions = { };

    # Path to traefik’s static configuration to use. (Using that option has precedence over staticConfigOptions and dynamicConfigOptions)
    staticConfigFile = /etc/nixos/modules/traefik/static_config.yml;
    # Static configuration for Traefik.
    # staticConfigOptions = {
    #   entryPoints = {
    #     http = {
    #       address = ":80";
    #     };
    #   };
    # };

    # Files to load as environment file. Environment variables from this file will be substituted into the static configuration file using envsubst.
    environmentFiles = [];
  };
}
