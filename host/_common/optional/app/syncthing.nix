{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    syncthing
    # syncthingtray # plasma
    # gnomeExtensions.syncthing-icon # gnome
  ];

  services.syncthing = {
    enable = true; # Whether to enable Syncthing, a self-hosted open-source alternative to Dropbox and Bittorrent Sync.
    systemService = true; # Whether to auto-launch Syncthing as a system service.
    #user = "myusername"; # The user to run Syncthing as. By default, a user named syncthing will be created whose home directory is dataDir.
    # dataDir = "/home/myusername/Documents";
    # configDir = "/home/myusername/Documents/.config/syncthing";
    overrideDevices = true;     # overrides any devices added or deleted through the WebUI
    overrideFolders = true;     # overrides any folders added or deleted through the WebUI
    settings = {
      devices = {
        "NAS" = { id = "AXOJRE7-72J4JIY-B52GI7U-INBUHI2-Q7REDR7-SFMAQDN-MN3UCWB-LXLU7QA"; };
      };
      folders = {
        "Keepass" = {        # Name of folder in Syncthing, also the folder ID
          path = "/home/admin.brucks/Syncthing/Keepass";    # Which folder to add to Syncthing
          devices = [ "NAS" ];      # Which devices to share the folder with
          type = "sendreceive";
          versioning = {
            type = "staggered";
            # fsPath = ".stversions";
            params = {
              cleanInterval = "3600";
              maxAge = "31536000";
            };
          };
        };
        "Logseq - Chris (Personal)" = {
          path = "/home/admin.brucks/Syncthing/Logseq/Chris (Personal)";
          devices = [ "NAS" ];
          type = "sendreceive";
          versioning = {
            type = "staggered";
            # fsPath = ".stversions";
            params = {
              cleanInterval = "3600";
              maxAge = "31536000";
            };
          };
        };
      };
    };
  };
}
