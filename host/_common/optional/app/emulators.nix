{ config, pkgs, ... }:

{
  services.xserver.desktopManager.retroarch = {
    enable = true;
    package = pkgs.retroarchFull;
    extraArgs = [
    ];
  };
}