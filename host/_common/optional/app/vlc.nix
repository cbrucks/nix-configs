{ config, pkgs, ... }:

let
  libbluray = pkgs.libbluray.override {
    withAACS = true;
    withBDplus = true;
    withJava = true;
  };

  vlc = pkgs.vlc.override { inherit libbluray; };
in
{
  environment.systemPackages = [ vlc ];
}