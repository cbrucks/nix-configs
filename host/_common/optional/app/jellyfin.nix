{ config, pkgs, ... }:

{
  users.users.jellyfin = {
    uid = 3000; # the same one I used for docker user
    isSystemUser = true;
    group = "jellyfin";
    description = "Primary user for jellyfin execution";
  };
  users.groups.jellyfin = {
    gid = 3000;
    members = [ "jellyfin" ];
  };

  services = {
    jellyfin = {
      package = pkgs.jellyfin;

      enable = true;
      openFirewall = true;

      group = "jellyfin";
      user = "jellyfin";

      configDir = "/etc/jellyfin";
      dataDir = "/var/lib/jellyfin";
      cacheDir = "/var/cache/jellyfin";
      logDir = "/var/log/jellyfin";
    };

    jellyseerr = {
      enable = false;
    };
  };

  # For mount.cifs, required unless domain name resolution is not needed.
  environment.systemPackages = [ pkgs.cifs-utils ];

  fileSystems."/mnt/jellyfin/nas/Media" = {
    device = "//nas/Media";
    fsType = "cifs";
      options = let
        # this line prevents hanging on network split
        automount_opts = "x-systemd.after=network-online.target,x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
        credentials = "credentials=/${config.services.jellyfin.configDir}/smb-nas-creds";
        permissions = "uid=3000,gid=3000,file_mode=0700,dir_mode=0700";
      in ["${automount_opts},${credentials},${permissions}"];
  };
}