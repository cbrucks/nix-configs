{ config, pkgs, ... }:

{
  imports = [
    ./docker.nix
  ];

  # Enable dynamic CDI configuration for Nvidia devices by running nvidia-container-toolkit on boot.
  hardware.nvidia-container-toolkit.enable = true;
}