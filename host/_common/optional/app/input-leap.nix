{ config, pkgs, ... }:

{
  # software KVM for sharing mouse, keyboard, and clipboard with other computers
  environment.systemPackages = [
    pkgs.input-leap
  ];
  
  # open the firewall ports
  networking.firewall = {
    allowedTCPPorts = [ 24800 ];
    allowedUDPPorts = [ 24800 ];
  };
}
