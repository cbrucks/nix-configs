{ config, pkgs, inputs, ... }:

{
  # imports = [
  #   inputs.nix-flatpak.nixosModules.nix-flatpak
  # ];

  # Application sandboxing and distribution framework.
  services.flatpak.enable = true;
  # A portal frontend service for Flatpak and other desktop containment frameworks.
  xdg.portal.enable = true;
}
