{ config, pkgs, ... }:

{
  # Install Docker (https://search.nixos.org/options?from=0&size=50&sort=alpha_asc&query=virtualisation.docker)
  virtualisation = {
    docker = {
      enable = true;

      # Use docker in rootless mode (https://docs.docker.com/engine/security/rootless/)
      rootless = {
        enable = true;
        # Sets the DOCKER_HOST variable to the rootless Docker instance for normal users by default.
        setSocketVariable = true;
        daemon.settings = {
          group = "docker";
        };
      };

      # Additional options
      # Required for "--restart=always"
      enableOnBoot = true;
      # Must be disabled for docker swarm
      liveRestore = false;
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.docker = {
    isNormalUser = true;
    isSystemUser = false;
    description = "docker user";
    group = "docker";
    uid = 2000;
    shell = null;
  };
}
