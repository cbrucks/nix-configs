{ config, pkgs, inputs, ... }:

{
  environment.systemPackages = with pkgs; [
    appimage-run
  ];
}
