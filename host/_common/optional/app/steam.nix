{ config, pkgs, ... }:

{
  # Install Steam`
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
    gamescopeSession = {
      enable = true;
      args = [];
      env = {};
    };
  };
  hardware.opengl.driSupport32Bit = true; # Enables support for 32bit libs that steam uses # deprecated

  # By default, Steam looks for custom Proton versions such as GE-Proton
  # in ~/.steam/root/compatibilitytools.d. Additionally the environment
  # variable STEAM_EXTRA_COMPAT_TOOLS_PATHS can be set to change or add
  # to the paths which steam searches for custom Proton versions. 

  # Install and manage Proton-GE and Luxtorpeda for Steam and Wine-GE for Lutris with this graphical user interface.
  environment.systemPackages = [
    pkgs.protonup-qt
  ];
}