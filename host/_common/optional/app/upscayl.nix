{ config, pkgs, ... }:

{
  # https://github.com/upscayl

  environment.systemPackages = with pkgs; [
    upscayl # AI Image Upscaler
  ];

  # Custom Models - download and reference from inside Upscayl application
  # https://github.com/upscayl/custom-models

}