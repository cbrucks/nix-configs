{ pkgs, config, ... }:
let
  dockerEnabled = config.virtualisation.docker.enable;
in
{
  # An implementation of docker-compose with podman backend
  environment.systemPackages = with pkgs; [
    podman-compose
        # User-mode networking for unprivileged network namespaces
        pkgs.slirp4netns
        # An implementation of overlay+shiftfs in FUSE for rootless containers.
        pkgs.fuse-overlayfs
  ];

  virtualisation = {
    podman = {
      # This option enables Podman, a daemonless container engine for developing, managing, and running OCI Containers on your Linux System.
      # It is a drop-in replacement for the docker command.
      enable = true;

      # extra packages required for rootless
      extraPackages = [ 
      ];

      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = !dockerEnabled;
      # Make the Podman socket available in place of the Docker socket, so Docker tools can find the Podman socket.
      # Podman implements the Docker API.
      # Users must be in the podman group in order to connect. As with Docker, members of this group can gain root access.
      dockerSocket.enable = !dockerEnabled;
      # Required for containers under podman-compose to be able to talk to each other.
      # For Nixos version > 22.11
      defaultNetwork.settings.dns_enabled = true;

      autoPrune = {
        # Whether to periodically prune Podman resources. If enabled, a systemd timer will run `podman system prune -f` as specified by the `dates` option.
        enable = true;
        # Specification (in the format described by systemd.time(7)) of the time at which the prune will occur.
        dates = "weekly";
      };

      # Make the Podman and Docker compatibility API available over the network with TLS client certificate authentication.
      # This allows Docker clients to connect with the equivalents of the Docker CLI -H and --tls* family of options.
      #networkSocket.enable = true;
    };

    oci-containers = {
      backend = "podman";

      # containers = {
      #   caddy = import ./containers/caddy.nix; # Add manually the Caddyfile before using this container
      #   homer = import ./containers/homer.nix;
      # };
    };
  };

  # systemd.services.create-podman-network = with config.virtualisation.oci-containers; {
  #   serviceConfig.Type = "oneshot";
  #   #wantedBy = [ "${backend}-homer.service" "${backend}-caddy.service" ];
  #   script = ''
  #     ${pkgs.podman}/bin/podman network exists net_macvlan || \
  #       ${pkgs.podman}/bin/podman network create --driver=macvlan --gateway=192.168..1 --subnet=192.168.xx.0/24 -o parent=ens18 net_macvlan
  #   '';
  # };

  users.users.podman = {
    uid = 2000; # the same one I used for docker user
    isSystemUser = true;
    group = "podman";
    description = "Primary user for podman execution";
  };
  users.groups.podman.members = [ "podman" ];

  # boot.kernel.sysctl = {
  #   "net.ipv4.ip_unprivileged_port_start" = 80;
  # };

  # a program with the CAP_NET_RAW capability
  #security.wrappers = {
  #  podman = {
  #    owner = "podman";
  #    group = "podman";
  #    capabilities = "cap_net_bind_service=+ep";
  #    source = "${pkgs.podman.out}/bin/podman";
  #  };
  #};

  # environment.persistence = {
  #   "/persist".directories = [
  #     "/var/lib/containers"
  #   ];
  # };
}
