{ config, pkgs, lib, ... }:

{
  boot.binfmt.emulatedSystems = [
    "i686-linux" # support 32-bit linux
  ];

  # This module adds a builder user that can be used to remotely build packages for slower machines
  users.users.builder = {
    isNormalUser = true;
    openssh.authorizedKeys.keys = [
          ];
  };
  nix.trustedUsers = [ "builder" ];
}