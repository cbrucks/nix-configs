{ config, pkgs, lib, ... }:

{
  nix.distributedBuilds = true;
  nix.buildMachines = let
    user = "builder";
    systems = [ "x86_64-linux" "i686-linux" ];
  in [
    {
      hostName = "desktop";
      supportedFeatures = [ "big-parallel" ]; # allow big compile tasks
      sshUser = ${user};
      systems = ${systems};
    }
    {
      hostName = "laptop";
      supportedFeatures = [ "big-parallel" ]; # allow big compile tasks
      sshUser = ${user};
      systems = ${systems};
    }
    {
      hostName = "laptop2";
      supportedFeatures = [ "big-parallel" ]; # allow big compile tasks
      sshUser = ${user};
      systems = ${systems};
    }
  ];
}