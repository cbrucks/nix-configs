# https://gitlab.com/-/snippets/2535355
# Authored by: https://gitlab.com/cbleslie (Cameron Leslie)

# Usage

# Import module.
# ./virtualisation/portainer-edge-agent.nix - or wherever  you're going to drop this.

# Add this nonsense to your config.
# {
#   services.portainer-edge-agent = {
#     enable = true;
#     id = "<ID FROM PORTAINER WIZARD>";
#     key = "<KEY FROM PORTAINER WIZARD";
#     version = "<VERSION OF DOCKER CONTAINER YOU WANT TO PULL DOWN>";
#   };
# }

{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.services.portainer-edge-agent;
in
{
  options.services.portainer-edge-agent = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        If enabled, run container edge agent
      '';
    };
    version = mkOption {
      type = types.str;
      default = 'latest';
      description = ''
        Sets the Edge Agent Container version.
      '';
    };
    id = mkOption {
      type = types.str;
      description = ''
        Sets the Edge Agent Edge ID.
      '';
    };
    key = mkOption {
      type = types.str;
      description = ''
        Sets the Edge Agent Edge Key.
      '';
    };
  };

  config = {
    virtualisation.oci-containers.containers.portainer-edge-agent = mkIf cfg.enable {
      image = "portainer/agent:${cfg.version}";
      volumes = [
        "/:/host"
        "/var/lib/docker/volumes:/var/lib/docker/volumes"
        "/var/run/docker.sock:/var/run/docker.sock"
        "portainer_agent_data:/data"
      ];
      autoStart = true;
      extraOptions = [
        "--pull=always"
        "--restart=unless-stopped"
        "--rm=false"
      ];
      environment = {
        EDGE = "1";
        EDGE_ID = cfg.id;
        EDGE_INSECURE_POLL = "1";
        EDGE_KEY = cfg.key;
      };
    };
    virtualisation.docker.enable = true;
    virtualisation.oci-containers.backend = "docker";
    networking.firewall.allowedTCPPorts = [ 9443 8000 ];
  };
}
