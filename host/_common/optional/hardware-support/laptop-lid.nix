{ lib, ... }:

{
  powerManagement = {
    # Whether to enable power management. This includes support for suspend-to-RAM and powersave features on laptops.
    enable = lib.mkDefault true;

    # Whether to enable powertop auto tuning on startup.
    powertop.enable = lib.mkDefault true;

    # Configure the governor used to regulate the frequency of the available CPUs.
    # "ondemand”, “powersave”, “performance”
    cpuFreqGovernor = lib.mkDefault "ondemand";
  };

  # Lid Settings
  services.logind = {
    lidSwitch = lib.mkDefault "suspend-then-hibernate";
    lidSwitchDocked = lib.mkDefault "ignore";
    lidSwitchExternalPower = lib.mkDefault "lock";

    powerKeyLongPress = lib.mkDefault "halt";
  };
}