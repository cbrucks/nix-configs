{ config, pkgs, ... }:

{
  hardware.graphics = {
    # Whether to enable hardware accelerated graphics drivers.
    #
    # This is required to allow most graphical applications and environments to use hardware rendering, video encode/decode acceleration, etc.
    #
    # This option should be enabled by default by the corresponding modules, so you do not usually have to set it yourself.
    enable = true;

    # On 64-bit systems, whether to also install 32-bit drivers for 32-bit applications (such as Wine).
    #enable32Bit = false;
  };
}