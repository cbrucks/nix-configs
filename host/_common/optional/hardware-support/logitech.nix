{ config, pkgs, ... }:

{
  # Logitech Wireless Devices
  hardware.logitech.wireless = {
    enable = true;
    enableGraphical = true;
  };
}