{ config, pkgs, inputs, ... }:

{
  programs.hyprland = {
    enable = true;

    # set the flake package
    #package = ;
    # make sure to also set the portal package, so that they are in sync
    #portalPackage = ;

    # Set environment path of systemd to include the current system’s bin directory.
    # This is needed in Hyprland setups, where opening links in applications do not work.
    # Enabled by default for Hyprland versions older than 0.41.2.
    #systemd.setPath.enable = true;

    xwayland.enable = true;
  };

  # Whether to enable hypridle, Hyprland’s idle daemon.
  services.hypridle.enable = true;

  # Whether to enable hyprlock, Hyprland’s GPU-accelerated screen locking utility.
  programs.hyprlock.enable = true;

  environment.systemPackages = with pkgs; [
    hyprland-protocols # Wayland protocol extensions for Hyprland
    hyprland-workspaces # A multi-monitor aware Hyprland workspace widget
    hyprlandPlugins.hyprbars # Hyprland window title plugin
    kitty # required for the default Hyprland config
  ];
}