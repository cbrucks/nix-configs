{ config, pkgs, ... }:

{
  environment = {
    gnome.excludePackages = with pkgs; [
      gnome-tour
      epiphany # web browser
      xterm # terminal emulator
      geary # email reader
      gnome-music # music player
      totem # video player
      #gnome-text-editor # text editor
      #gnome-photos
      #cheese # webcam tool
      #evince # document viewer
      #gnome-calendar
      #gnome-contacts # contacts
      #gnome-maps # navigation app
      gnome-software # software store
      #gnome-terminal
      #gnome-weather # current weather conditions and forecasts
      #seahorse # manage keys and passwords
      #simple-scan # scanning utility
      #yelp # help viewer
    ];

    systemPackages = (with pkgs; [
      cifs-utils # network shares
      dconf2nix # convert dconf settings to nix home-manager settings. Ex: dconf dump / | dconf2nix > dconf.nix
      dconf-editor
      adwaita-icon-theme
      gnome-tweaks
      gnome-calculator
      gnuchess
    ]) ++ (with pkgs.gnomeExtensions; [
      allow-locked-remote-desktop # Allow remote desktop connections when the screen is locked
    ]);

    variables = rec {
      # Ignore XDG_SESSION_TYPE=wayland on Gnome and run on Wayland anyway.
      QT_QPA_PLATFORM = "wayland";
    };
  };

  services = {
    # required for systray icons
    udev.packages = with pkgs; [ gnome-settings-daemon ];

    xserver = {
      # Enable the X11 windowing system.
      enable = true;

      # Gnome Desktop Manager.
      desktopManager.gnome.enable = true;
    };

    gnome = {
      # Native host connector for the GNOME Shell browser extension, a DBus service allowing
      # to install GNOME Shell extensions from a web browser.
      gnome-browser-connector.enable = true;

      # Remote Desktop support using Pipewire
      gnome-remote-desktop.enable = true;

      # GNOME games
      games.enable = true;

      # A quick previewer for nautilus.
      sushi.enable = true;

      # GNOME Keyring
      gnome-keyring.enable = true;
    };
  };

  programs = {
    # Whether to enable Xwayland (an X server for interfacing X11 apps with the Wayland protocol).
    xwayland.enable = true;

    kdeconnect = {
      enable = true;
      package = pkgs.gnomeExtensions.gsconnect;
    };
  };
}
