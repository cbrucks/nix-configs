{ config, pkgs, lib, ... }:

{
  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.desktopManager.plasma6.enable = true;
  # Set X11 session as your default session
  #services.xserver.displayManager.defaultSession = "plasmax11";
  # force a pure Qt 6 system
  #services.desktopManager.plasma6.enableQt5Integration = false;

  services.xrdp.enable = true;
  services.xrdp.defaultWindowManager = "startplasma-x11";
  #services.xrdp.defaultWindowManager = "startplasma-wayland";
  services.xrdp.openFirewall = true;

  environment.plasma6.excludePackages = with pkgs.kdePackages; [
  ];

  environment.systemPackages = (with pkgs; [
    kate       # Text editor
    kcalc      # Calculator
    xorg.xkill # X-Org application to easily kill a window by clicking on it
  ]) ++ (with pkgs.kdePackages; [
    discover   # KDE and Plasma software management GUI
  ]);

  # Enable Qt Configuration
  qt = {
    enable = true;
    platformTheme = lib.mkDefault "kde";
    style = lib.mkDefault "breeze";
  };
  # Fixes GTK themes not being applied in Wayland applications
  # programs.dconf.enable = true;

  programs = {
    partition-manager.enable = true;
    kclock.enable = true;
    
    # Whether to enable Xwayland (an X server for interfacing X11 apps with the Wayland protocol).
    xwayland.enable = true;

    kdeconnect = {
      enable = true;
    };
  };
}
