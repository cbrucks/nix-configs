{ config, pkgs, ... }:

{
  # imports =
  #   [
  #     ./dm/lightdm.nix
  #   ];
  
  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the Pantheon Desktop Environment.
  services.xserver.desktopManager.pantheon.enable = true;
}
