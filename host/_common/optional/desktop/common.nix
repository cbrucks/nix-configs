{ config, pkgs, lib, ... }:

{
  # Enable CUPS to print documents.
  services.printing.enable = true;

  environment.sessionVariables = rec {
    # Turn on native wayland support in most electron apps (since NixOS 22.05)
    NIXOS_OZONE_WL = "1";
    # Instructs electron apps to try wayland first, then fallback to x11
    ELECTRON_OZONE_PLATFORM_HINT = "auto";
  };

  hardware.pulseaudio.enable = false;
  # RealtimeKit system service, which hands out realtime scheduling priority to user processes on demand.
  # For example, the PulseAudio server uses this to acquire realtime priority.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };
}
