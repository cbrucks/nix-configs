{ config, pkgs, ... }:

{
  # Disable the Desktop Environment.
  services.xserver.enable = false;
}
