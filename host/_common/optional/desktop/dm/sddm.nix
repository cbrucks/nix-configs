{ pkgs, lib, ... }:

{
  imports =
    [
      ../common.nix
    ];
  
  # Enable the X11 windowing system.
  services.xserver = {
    enable = lib.mkDefault true;
  
    displayManager = {
      # Shell commands executed just after the X server has started.
      # This option is only effective for display managers for which this feature is supported; currently these are LightDM, GDM and SDDM.
      setupCommands = "";
      # Shell commands executed just before the window or desktop manager is started. These commands are not currently sourced for Wayland sessions.
      sessionCommands = "";
    };
  };

  services.displayManager.sddm = {
    # Whether to enable sddm as the display manager.
    enable = true;

    wayland = {
      # Whether to enable experimental Wayland support.
      enable = true;

      # The compositor to use: kwin, weston
      #compositor = "weston";
    };

    # Greeter theme to use.
    #theme = "";

    # A script to execute when stopping the display server.
    #stopScript = "";

    # Extra settings merged in and overwriting defaults in sddm.conf.
    # attribute set of section of an INI file (attrs of INI atom (null, bool, int, float or string))
    #settings = { };

    # The sddm package to use.
    #package = pkgs.plasma5Packages.sddm;

    # Extra Qt plugins / QML libraries to add to the environment.
    #extraPackages = "[]";

    # Whether to enable automatic HiDPI mode.
    #enableHidpi = true;

    # Enable numlock at login.
    autoNumlock = true;
  };
}
