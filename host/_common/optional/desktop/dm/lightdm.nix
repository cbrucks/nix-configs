{ pkgs, lib, ... }:

{
  imports =
    [
      ../common.nix
    ];

  # Enable the X11 windowing system.
  services.xserver = {
    enable = lib.mkDefault true;

    displayManager = {
      # Shell commands executed just after the X server has started.
      # This option is only effective for display managers for which this feature is supported; currently these are LightDM, GDM and SDDM.
      setupCommands = "";
      # Shell commands executed just before the window or desktop manager is started. These commands are not currently sourced for Wayland sessions.
      sessionCommands = "";

      lightdm = {
        # Allow GDM to run on Wayland instead of Xserver
        # wayland = true;

        # Gnome Display Manager.
        enable = true;
      };
    };
  };
}
