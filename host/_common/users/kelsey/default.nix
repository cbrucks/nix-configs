{ config, pkgs, ... }:

let
  ifTheyExist = groups: builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
in
{
  users.mutableUsers = false;

  # Define a user account.
  users.users.kelsey = {
    isNormalUser = true;
    description = "kelsey";
    # Can be updated by using `mkpasswd` on the command line if users.mutableUsers is true
    hashedPassword = "$y$j9T$nT3tuGpLw0zS2If1GiAKo0$ZXYnycLi4z7xZQbew3ZF.lNMr7XPzqpMTzLCnr.1xw7";
    # hashedPasswordFile = config.sops.secrets.kelsey-password.path;
    extraGroups = [
    ] ++ ifTheyExist [
      "network"
      "networkmanager"
    ];
    openssh.authorizedKeys.keys = [];
    # shell = pkgs.fish;
    packages = [ pkgs.home-manager ];
  };

  # sops.secrets.kelsey-password = {
  #   sopsFile = ../../secrets.yaml;
  #   neededForUsers = true;
  # };

  home-manager.users.kelsey.imports = [
    ../../../../home/kelsey/${config.networking.hostName}.nix
  ];
}
