{ config, pkgs, ... }:

let
  ifTheyExist = groups: builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
in
{
  users.mutableUsers = false;

  # Define a user account.
  users.users.hannah = {
    isNormalUser = true;
    description = "hannah";
    # Can be updated by using `mkpasswd` on the command line if users.mutableUsers is true
    hashedPassword = "$y$j9T$Fesb8tzor5fc6w726np/i0$a.VFoWrfdNZXXDKFBxTl7EbupZjDzWCWFmf52KMlif4";
    # hashedPasswordFile = config.sops.secrets.hannah-password.path;
    extraGroups = [
    ] ++ ifTheyExist [
      "network"
      "networkmanager"
    ];
    openssh.authorizedKeys.keys = [];
    # shell = pkgs.fish;
    packages = [ pkgs.home-manager ];
  };

  # sops.secrets.hannah-password = {
  #   sopsFile = ../../secrets.yaml;
  #   neededForUsers = true;
  # };

  home-manager.users.hannah.imports = [
    ../../../../home/hannah/${config.networking.hostName}.nix
  ];
}
