{ config, pkgs, ... }:

let
  # this line prevents hanging on network split
  automount_opts = "x-systemd.after=tailscale.service,x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
  credentials = "credentials=/home/chris/nas/credentials";
  permissions = "uid=1002,gid=100,file_mode=0700,dir_mode=0700";
in {
  # For mount.cifs, required unless domain name resolution is not needed.
  environment.systemPackages = [ pkgs.cifs-utils ];

  fileSystems."/home/chris/nas/user" = {
    device = "//nas/Users/chris";
    fsType = "cifs";
    options = ["${automount_opts},${credentials},${permissions}"];
  };

  fileSystems."/home/chris/nas/shared" = {
    device = "//nas/Users/shared";
    fsType = "cifs";
    options = ["${automount_opts},${credentials},${permissions}"];
  };

  fileSystems."/home/chris/nas/games" = {
    device = "//nas/Games";
    fsType = "cifs";
    options = ["${automount_opts},${credentials},${permissions}"];
  };

  fileSystems."/home/chris/nas/media" = {
    device = "//nas/Media";
    fsType = "cifs";
    options = ["${automount_opts},${credentials},${permissions}"];
  };

  fileSystems."/home/chris/nas/software" = {
    device = "//nas/Software";
    fsType = "cifs";
    options = ["${automount_opts},${credentials},${permissions}"];
  };

  fileSystems."/home/chris/nas/docker" = {
    device = "//nas/docker";
    fsType = "cifs";
    options = ["${automount_opts},${credentials},${permissions}"];
  };

  networking.firewall.extraCommands = ''iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns'';
}

