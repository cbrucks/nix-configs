{ config, pkgs, ... }:

let
  ifTheyExist = groups: builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
in
{
  imports = [
    ./smb-nas.nix
  ];

  users.mutableUsers = false;

  # Define a user account.
  users.users.chris = {
    isNormalUser = true;
    description = "chris";
    uid = 1002;
    home = "/home/chris";
    # Can be updated by using `mkpasswd` on the command line if users.mutableUsers is true
    hashedPassword = "$y$j9T$.IGxU2UZhl8YrPIg8MIOp/$m9jUFaOQhfpbmnIwOPR4xfSuXhDhE0r4C2A/U//En20";
    # hashedPasswordFile = config.sops.secrets.chris-password.path;
    extraGroups = [
      "wheel"
      "audio"
      "video"
    ] ++ ifTheyExist [
      "network"
      "networkmanager"
      "docker"
      "podman"
      "git"
      "libvirtd"
      "input"
    ];
    openssh.authorizedKeys.keys = [];
    shell = pkgs.fish;
    packages = [ pkgs.home-manager ];
  };

  programs.fish.enable = true;

  # sops.secrets.chris-password = {
  #   sopsFile = ../../secrets.yaml;
  #   neededForUsers = true;
  # };

  home-manager.users.chris.imports = [
    ../../../../home/chris/${config.networking.hostName}.nix
  ];
}
