{ config, pkgs, ... }:

{
  # Define a user account.
  users.users."admin.chris" = {
    isNormalUser = true;
    description = "admin-chris";
    # Can be updated by using `mkpasswd` on the command line
    hashedPassword = "$y$j9T$.IGxU2UZhl8YrPIg8MIOp/$m9jUFaOQhfpbmnIwOPR4xfSuXhDhE0r4C2A/U//En20";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
    ];

    # SSH Public Keys
    openssh.authorizedKeys.keys = [
    ];
  };
}
