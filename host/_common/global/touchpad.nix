{ lib, ... }:

{
  # Enable touchpad support (enabled default in most desktopManager).
  # Options: https://search.nixos.org/options?query=services.xserver.libinput
  services.libinput = {
    enable = true;
    touchpad = {
      naturalScrolling = true;
      tapping = true;
      disableWhileTyping = true;
      tappingButtonMap = "lrm";
    };
  };
}