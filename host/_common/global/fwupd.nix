{ config, pkgs, lib, ... }:

{
  # A DBus service that allows applications to update firmware
  services.fwupd = {
    # enable fwupd
    enable = true;

  };
}
