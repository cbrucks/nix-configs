{ config, pkgs, ... }:

{
  # Clam AntiVirus
  services.clamav = {
    # ClamAV clamd daemon
    daemon = {
      enable = true;

      # ClamAV configuration
      # Refer to https://linux.die.net/man/5/clamd.conf, for details on supported values.
      settings = {
        # TODO review add clamd settings
      };
    };

    # ClamAV freshclam updater
    updater = {
      enable = true;

      # Number of database checks per day (default: 12)
      frequency = 12;

      # How often freshclam is invoked
      # See systemd.time(7) for more information about the format.
      interval = "hourly";

      # Freshclam configuration
      # Refer to https://linux.die.net/man/5/freshclam.conf, for details on supported values.
      settings = {
        # TODO review add freshclam settings
      };
    };
  };

  environment.systemPackages = [
    pkgs.clamtk # Front end for ClamAV
  ];
}
