{ lib, ... }:

{
  i18n = {
    # The default locale. It determines the language for program messages, the format for dates and times,
    # sort order, and so on. It also determines the character set, such as UTF-8.
    defaultLocale = lib.mkDefault "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS        = lib.mkDefault "en_US.UTF-8";
      LC_IDENTIFICATION = lib.mkDefault "en_US.UTF-8";
      LC_MEASUREMENT    = lib.mkDefault "en_US.UTF-8";
      LC_MONETARY       = lib.mkDefault "en_US.UTF-8";
      LC_NAME           = lib.mkDefault "en_US.UTF-8";
      LC_NUMERIC        = lib.mkDefault "en_US.UTF-8";
      LC_PAPER          = lib.mkDefault "en_US.UTF-8";
      LC_TELEPHONE      = lib.mkDefault "en_US.UTF-8";
      LC_TIME           = lib.mkDefault "en_US.UTF-8";
    };
    supportedLocales = lib.mkDefault [
      "en_US.UTF-8/UTF-8"
    ];
  };

  time.timeZone = lib.mkDefault "America/Chicago";

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };
}