{ config, pkgs, lib, ... }:

{
  # This installs the service
  # In order for the device to connect to the tailnet, run one of the following commands
  #   laptops:
  #     sudo tailscale up --reset --ssh --accept-routes
  #   servers:
  #     sudo tailscale up --reset --ssh --advertise-exit-node --exit-node-allow-lan-access
  #
  # SSH'ing into another machine by running the following command
  #   tailscale ssh user@machine
  services.tailscale = {
    # Whether to enable Tailscale client daemon.
    enable = true;

    # Enables settings required for Tailscale’s routing features like subnet routers and exit nodes.
    # To use these these features, you will still need to call `sudo tailscale up` with the relevant flags like --advertise-exit-node and --exit-node.
    # "none", "client", "server", "both"
    # useRoutingFeatures = lib.mkDefault "client";

    # Whether to open the firewall for the specified port.
    # openFirewall = true;

    # Extra flags to pass to tailscale up.
    # extraUpFlags = lib.mkDefault [
    #   "--ssh"
    # ];

    # A file containing the auth key.
    # authKeyFile = lib.mkDefault "/run/secrets/tailscale_key";
  };

  # Required in order to make use of Tailscale MagicDNS
  networking = {
    nameservers = [ "100.100.100.100" "1.1.1.1" "1.0.0.1" ];
    search = [ "bearded-lime.ts.net" "brucks.local" ];
  };

  # environment.persistence = {
  #   "/persist".directories = [ "/var/lib/tailscale" ];
  # };
}
