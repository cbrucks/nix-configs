{ config, outputs, lib, ... }:

let
  #inherit (config.networking) hostName;
  #hosts = outputs.nixosConfigurations;
  #pubKey = host: ../../${host}/ssh_host_ed25519_key.pub;
  #gitHost = hosts."alcyone".config.networking.hostName;

  # Sops needs acess to the keys before the persist dirs are even mounted; so
  # just persisting the keys won't work, we must point at /persist
  #hasOptinPersistence = config.environment.persistence ? "/persist";
in
{
  services.openssh = {
    # Whether to enable the OpenSSH secure shell daemon, which allows secure remote logins.
    enable = true;

    # Configuration for sshd_config(5).
    settings = {
      # require public key authentication for better security
      #PasswordAuthentication = false;
      #KbdInteractiveAuthentication = false;

      # Whether the root user can login using ssh.
      PermitRootLogin = "no";
    };

    # Specifies on which ports the SSH daemon listens.
    ports = lib.mkDefault [ 22 ];
    # Whether to automatically open the specified ports in the firewall.
    openFirewall = true;

    #hostKeys = [{
    #  path = "${lib.optionalString hasOptinPersistence "/persist"}/etc/ssh/ssh_host_ed25519_key";
    #  type = "ed25519";
    #}];

    # Each hosts public key
    # knownHosts = builtins.mapAttrs
    #   (name: _: {
    #     publicKeyFile = pubKey name;
    #     extraHostNames =
    #       (lib.optional (name == hostName) "localhost") ++ # Alias for localhost if it's the same host
    #       (lib.optionals (name == gitHost) [ "m7.rs" "git.m7.rs" ]); # Alias for m7.rs and git.m7.rs if it's the git host
    #   })
    #   hosts;
  };

  # Passwordless sudo when SSH'ing with keys
  #security.pam.sshAgentAuth.enable = true;

  # This still allows to login with user writable keys but not to use them for privilege elevation.
  # See https://github.com/NixOS/nixpkgs/issues/31611
  #security.pam.sshAgentAuth.authorizedKeysFiles = lib.mkForce [ "/etc/ssh/authorized_keys.d/%u" ];
}
