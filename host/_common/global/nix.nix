{ inputs, lib, ... }:

{
  #system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;

  nix = {
    settings = {
      trusted-users = [ "root" "@wheel" ];
      auto-optimise-store = lib.mkDefault true;
      experimental-features = [ "nix-command" "flakes" ];
      warn-dirty = true;

      # Enable nixos caches
      substituters = [
        "https://nix-community.cachix.org"
        "https://cache.nixos.org/"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
    };

    # registry.nixpkgs.flake = nixpkgs;

    # Run the garbage collector regularly
    gc = {
      automatic = true;
      dates = "weekly";
      # Keep the last 10 generations
      options = "--delete-older-than 60d";
    };

    optimise = {
      automatic = true;
      dates = [
        "weekly"
      ];
    };

    # Add each flake input as a registry
    # To make nix3 commands consistent with the flake
    #registry = lib.mapAttrs (_: value: { flake = value; }) inputs;

    # Add nixpkgs input to NIX_PATH
    # This lets nix2 commands still use <nixpkgs>
    #nixPath = [ "nixpkgs=${inputs.nixpkgs.outPath}" ];
  };
}