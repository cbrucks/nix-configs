{ lib, config, pkgs, inputs, outputs, ... }:

{
  # The linux kernel comes with a basic xpad driver for Xbox 360 and Xbox One controllers,
  # however the functionality of this driver is very limited.

  boot.extraModprobeConfig = ''
    blacklist xpad
  '';

  # xone provides wired and wireless dongle support.
  hardware.xone.enable = true;

  # xpadneo provides bluetooth support
  hardware.xpadneo.enable = true;

  # These drivers may conflict and/or not work correctly with some 3rd party controllers.

  # These drivers are also specifically for the Xbox One controller, not the Xbox 360 controller.

  # In some newer Xbox Series X|S controllers bluetooth may go into a connect/disconnect loop.
  # If this happens, you will need to connect your controller to an Xbox or Windows PC and update
  # the firmware on the controller. After updating the firmware it should connect properly via bluetooth.
}