{ lib, ... }:

{
  boot.loader = {
    # Timeout (in seconds) until loader boots the default menu item. Use null if the loader menu should be displayed indefinitely.
    timeout = lib.mkDefault 5;

    systemd-boot = {
      # Whether to enable the systemd-boot (formerly gummiboot) EFI boot manager
      enable = true;

      # Whether to allow editing the kernel command-line before boot.
      editor = false;

      # The resolution of the console.
      consoleMode = "max";

      # Maximum number of latest generations in the boot menu. Useful to prevent boot partition running out of disk space.
      configurationLimit = 30;

      # Make Memtest86+ available from the systemd-boot menu. Memtest86+ is a program for testing memory.
      memtest86.enable = true;
    };

    # Whether the installation process is allowed to modify EFI boot variables.
    efi.canTouchEfiVariables = true;
  };
}
