{ lib, config, pkgs, inputs, outputs, ... }:

{
  imports = [ 
    inputs.home-manager.nixosModules.home-manager
    inputs.sops-nix.nixosModules.sops

    ./clamav.nix
    ./fwupd.nix
    ./gamemode.nix
    ./locale.nix
    ./nix.nix
    ./openssh.nix
    #./sops.nix
    ./systemd-boot.nix
    ./touchpad.nix
    ./tailscale.nix
    ./xbox-controller.nix
  ];

  # List available options
  #  nix repl
  #  nix-repl> :l <nixpkgs>
  #  nix-repl> pkgs.linuxPackages
  boot.kernelPackages = pkgs.linuxPackages;
  # boot.kernelPackages = pkgs.linuxPackages_latest;

  # Enable sysrq features
  # Use Alt+PrintScreen+(REISUB) to cleanly restart frozen computer
  boot.kernel.sysctl = {
    #   2 – enable control of console logging level
    #   4 – enable control of keyboard (SAK, unraw)
    #   8 – enable debugging dumps of processes etc.
    #  16 – enable sync command
    #  32 – enable remount read-only
    #  64 – enable signalling of processes (term, kill, oom-kill)
    # 128 – allow reboot/poweroff
    # 256 – allow nicing of all RT tasks
    "kernel.sysrq" = 502; # Enable everything except debugging dumps of processes (bit mask 8)
  };

  # 
  boot.kernelModules = [
    "sg" # required for optical drives to be recognized properly
  ];

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;

    # Optionally, use home-manager.extraSpecialArgs to pass arguments to home.nix
    extraSpecialArgs = { inherit inputs outputs; };
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Whether to enable firmware with a license allowing redistribution.
  hardware.enableRedistributableFirmware = true;

  # Enable networking
  networking.networkmanager.enable = lib.mkDefault true;

  # Enable the firewall
  networking.firewall.enable = lib.mkDefault true;

  # the contents of /etc/passwd and /etc/group wilexperimentall be congruent to your NixOS configuration
  users.mutableUsers = false;

  # Setup auto upgrade for the system
  system.autoUpgrade = {
    enable = true;
    operation = "boot";
    # Change the default schedule (must follow systemd.time format https://www.freedesktop.org/software/systemd/man/systemd.time.html)
    dates = "weekly";
  };

  # Add root CA cert for home network
  security.pki.certificates = [
    ''
      brucks.ddns.net
      ===============
      -----BEGIN CERTIFICATE-----
      MIIDQTCCAikCFFhtcJdSwV/MTcmGkhYhshD2yetDMA0GCSqGSIb3DQEBDQUAMF0x
      CzAJBgNVBAYTAlVTMQ4wDAYDVQQIDAVUZXhhczETMBEGA1UEBwwKUm91bmQgUm9j
      azEPMA0GA1UECgwGQnJ1Y2tzMRgwFgYDVQQDDA9icnVja3MuZGRucy5uZXQwHhcN
      MjMwMjIxMDQxNzEzWhcNMjYwMjIwMDQxNzEzWjBdMQswCQYDVQQGEwJVUzEOMAwG
      A1UECAwFVGV4YXMxEzARBgNVBAcMClJvdW5kIFJvY2sxDzANBgNVBAoMBkJydWNr
      czEYMBYGA1UEAwwPYnJ1Y2tzLmRkbnMubmV0MIIBIjANBgkqhkiG9w0BAQEFAAOC
      AQ8AMIIBCgKCAQEAuJWho6Zk8KqiV5u/vPRWTDPjQBFGSaembGoD832WIR4/z/2P
      OKCzAMjpOvkIZN3EVT9JSwOulcz8UAxGirMLxLoWD5pXpQlwx3j9cWE1RiBScn03
      qNnZhdNfhWltoAT/lq9oRdJHwiROYsC7Z7ljtKOffn7WefIsBv8fY8WGXL/yx81n
      R0FgPQF4FHKAqctIVF3Et68qt4e60eM60u+qZtFUd0cZQBIEF5AI0gMDrdKFAdEO
      yAMG9ybeRqKAP4SYgy4kvynHQC0OBBiHry/NtLx6grp8Wf7RQThMeKWKQXL/HXHw
      tDw8/wdtWC90b2aYlxAsftf7irP22e0ICHBfQwIDAQABMA0GCSqGSIb3DQEBDQUA
      A4IBAQCRZbarsfw70ziBDvQUxuZpY2MiQFv/WbwOL0dlct6FXc5HFAap6/pvRIAi
      xv81b79FrzkdwG+u20pSn9FjlYlddPy7hRp1KJ0lrsP4Vt2pQpIzRVXHIwO7S8Rq
      QtKI1kNNwZjuzCkwUvV63Qvm3ZJ2nKkIv+LWbXx30adGR4l28bnCSiCKEeydNELO
      om/j4IuAT6OC0WO+0y0PQ2CKqfJgHs8POiIWdRLmZKP91lZaTVH6rzAmokNYOg43
      iw41C1bP8thVNeaWBmkGla5WatDogsHI/yI9pcQX0COpJLObKMtZS4vI6Tzw22ka
      eRBtGW8CSqfITdeugqdmyHKmB9tH
      -----END CERTIFICATE-----
    ''
  ];
}
