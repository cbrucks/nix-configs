# Shell for benchmarking a system
{ pkgs ? # If pkgs is not defined, instanciate nixpkgs from locked commit
  let
    lock = (builtins.fromJSON (builtins.readFile ./flake.lock)).nodes.nixpkgs.locked;
    nixpkgs = fetchTarball {
      url = "https://github.com/nixos/nixpkgs/archive/${lock.rev}.tar.gz";
      sha256 = lock.narHash;
    };
  in
  import nixpkgs { overlays = [ ]; }
, ...
}: {
  default = pkgs.mkShell {
    nativeBuildInputs = with pkgs; [
      glmark2 # open source GPU benchmark
      # mprime  # CPU stress test
      stress  # CPU benchmark; workload generator (also useful for memory, I/O, and disk stress)
      s-tui   # Text user interface frontend for stress
    ];
  };

  # Examples:
  #
  # glmark2 --size 1920x1080
  #
  # stress --cpu 20
}