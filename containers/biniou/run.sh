#!/bin/sh

cd "$(dirname "$0")"

set -a
export BINIOU_UID=$(id -u)
export BINIOU_GUID=$(id -g)

docker compose up -d

# comment out `--gpus all` to remove CUDA support
# docker run -it --restart=always \
#   --hostname biniou \
#   --name biniou \
#   --device nvidia.com/gpu=all \
#   -p 7860:7860 \
#   -v ~/biniou/outputs:/home/biniou/biniou/outputs \
#   -v ~/biniou/models:/home/biniou/biniou/models \
#   -v ~/biniou/huggingface:/home/biniou/.cache/huggingface \
#   -v ~/biniou/gfpgan:/home/biniou/biniou/gfpgan \
#   biniou:latest

echo "Access the webui via the following url:"
echo "    light theme - https://127.0.0.1:7860"
echo "    dark  theme - https://127.0.0.1:7860/?__theme=dark"