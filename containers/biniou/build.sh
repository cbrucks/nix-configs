#!/bin/sh

# Without CUDA support
#sudo docker build -t biniou https://github.com/Woolverine94/biniou.git

# With CUDA support
docker build -t biniou https://raw.githubusercontent.com/Woolverine94/biniou/main/CUDA/Dockerfile
