#!/bin/sh

cd "$(dirname "$0")"

set -a
export BINIOU_UID=$(id -u)
export BINIOU_GUID=$(id -g)

docker compose down

# docker stop biniou
# docker rm biniou