#!/bin/sh

cd "$(dirname "$0")"

echo "Access the webui via the following url:"
echo "    http://0.0.0.0:7860"

docker compose up