#!/bin/sh

base_dir=/home/chris/comfyui/models

mkdir -p $base_dir

# Checkpoints

echo "Downloading checkpoints..."
# SDXL
wget -N --show-progress -c https://huggingface.co/stabilityai/stable-diffusion-xl-base-1.0/resolve/main/sd_xl_base_1.0.safetensors -P $base_dir/checkpoints/
wget -N --show-progress -c https://huggingface.co/stabilityai/stable-diffusion-xl-refiner-1.0/resolve/main/sd_xl_refiner_1.0.safetensors -P $base_dir/checkpoints/
# wget -N --show-progress -c https://huggingface.co/stabilityai/stable-diffusion-xl-base-1.0/resolve/main/sd_xl_base_1.0_0.9vae.safetensors -P $base_dir/checkpoints/

# SVD
wget -N --show-progress -c https://huggingface.co/stabilityai/stable-video-diffusion-img2vid/resolve/main/svd.safetensors -P $base_dir/checkpoints/
wget -N --show-progress -c https://huggingface.co/Lightricks/LTX-Video/resolve/main/ltx-video-2b-v0.9.1.safetensors -P $base_dir/checkpoints/

# SD1.5
# wget -N --show-progress -c https://huggingface.co/runwayml/stable-diffusion-v1-5/resolve/main/v1-5-pruned-emaonly.ckpt -P $base_dir/checkpoints/
wget -N --show-progress -c https://huggingface.co/stabilityai/stable-diffusion-2-1-base/resolve/main/v2-1_512-ema-pruned.safetensors -P $base_dir/checkpoints/
# wget -N --show-progress -c https://huggingface.co/stabilityai/stable-diffusion-2-1/resolve/main/v2-1_768-ema-pruned.safetensors -P $base_dir/checkpoints/
# Some SD1.5 anime style
# wget -N --show-progress -c https://huggingface.co/WarriorMama777/OrangeMixs/resolve/main/Models/AbyssOrangeMix2/AbyssOrangeMix2_hard.safetensors -P $base_dir/checkpoints/
# wget -N --show-progress -c https://huggingface.co/WarriorMama777/OrangeMixs/resolve/main/Models/AbyssOrangeMix3/AOM3A1_orangemixs.safetensors -P $base_dir/checkpoints/
# wget -N --show-progress -c https://huggingface.co/WarriorMama777/OrangeMixs/resolve/main/Models/AbyssOrangeMix3/AOM3A3_orangemixs.safetensors -P $base_dir/checkpoints/
# wget -N --show-progress -c https://huggingface.co/Linaqruf/anything-v3.0/resolve/main/anything-v3-fp16-pruned.safetensors -P $base_dir/checkpoints/
# Waifu Diffusion 1.5 (anime style SD2.x 768-v)
# wget -N --show-progress -c https://huggingface.co/waifu-diffusion/wd-1-5-beta2/resolve/main/checkpoints/wd-1-5-beta2-fp16.safetensors -P $base_dir/checkpoints/
# unCLIP models
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/illuminatiDiffusionV1_v11_unCLIP/resolve/main/illuminatiDiffusionV1_v11-unclip-h-fp16.safetensors -P $base_dir/checkpoints/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/wd-1.5-beta2_unCLIP/resolve/main/wd-1-5-beta2-aesthetic-unclip-h-fp16.safetensors -P $base_dir/checkpoints/
# ---
# VAE
wget -N --show-progress -c https://huggingface.co/stabilityai/sd-vae-ft-mse-original/resolve/main/vae-ft-mse-840000-ema-pruned.safetensors -P $base_dir/vae/
# wget -N --show-progress -c https://huggingface.co/WarriorMama777/OrangeMixs/resolve/main/VAEs/orangemix.vae.pt -P $base_dir/vae/
# wget -N --show-progress -c https://huggingface.co/hakurei/waifu-diffusion-v1-4/resolve/main/vae/kl-f8-anime2.ckpt -P $base_dir/vae/
# Loras
# wget -N --show-progress -c https://civitai.com/api/download/models/10350 -O $base_dir/loras/theovercomer8sContrastFix_sd21768.safetensors #theovercomer8sContrastFix SD2.x 768-v
# wget -N --show-progress -c https://civitai.com/api/download/models/10638 -O $base_dir/loras/theovercomer8sContrastFix_sd15.safetensors #theovercomer8sContrastFix SD1.x
# T2I-Adapter
# wget -N --show-progress -c https://huggingface.co/TencentARC/T2I-Adapter/resolve/main/models/t2iadapter_depth_sd14v1.pth -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/TencentARC/T2I-Adapter/resolve/main/models/t2iadapter_seg_sd14v1.pth -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/TencentARC/T2I-Adapter/resolve/main/models/t2iadapter_sketch_sd14v1.pth -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/TencentARC/T2I-Adapter/resolve/main/models/t2iadapter_keypose_sd14v1.pth -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/TencentARC/T2I-Adapter/resolve/main/models/t2iadapter_openpose_sd14v1.pth -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/TencentARC/T2I-Adapter/resolve/main/models/t2iadapter_color_sd14/v1.pth -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/TencentARC/T2I-Adapter/resolve/main/models/t2iadapter_canny_sd14v1.pth -P $base_dir/controlnet/
# T2I Styles Model
# wget -N --show-progress -c https://huggingface.co/TencentARC/T2I-Adapter/resolve/main/models/t2iadapter_style_sd14v1.pth -P $base_dir/style_models/
# CLIPVision model (needed for styles model)
# wget -N --show-progress -c https://huggingface.co/openai/clip-vit-large-patch14/resolve/main/pytorch_model.bin -O $base_dir/clip_vision/clip_vit14.bin
# ControlNet
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11e_sd15_ip2p_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11e_sd15_shuffle_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_canny_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11f1p_sd15_depth_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_inpaint_fp16.safetensors -P $base_dir/controlnet/
wget -N --show-progress -c https://huggingface.co/thibaud/controlnet-sd21/resolve/main/control_v11p_sd21_lineart.safetensors -P $base_dir/controlnet/
#wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_lineart_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_mlsd_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_normalbae_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_openpose_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_scribble_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_seg_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15_softedge_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11p_sd15s2_lineart_anime_fp16.safetensors -P $base_dir/controlnet/
# wget -N --show-progress -c https://huggingface.co/comfyanonymous/ControlNet-v1-1_fp16_safetensors/resolve/main/control_v11u_sd15_tile_fp16.safetensors -P $base_dir/controlnet/
wget -N --show-progress -c https://huggingface.co/thibaud/controlnet-openpose-sdxl-1.0/resolve/main/OpenPoseXL2.safetensors -P $base_dir/controlnet/

# https://huggingface.co/stabilityai/control-lora
wget -N --show-progress -c https://huggingface.co/stabilityai/control-lora/resolve/main/control-LoRAs-rank256/control-lora-N --show-progress -canny-rank256.safetensors -P $base_dir/controlnet/
wget -N --show-progress -c https://huggingface.co/stabilityai/control-lora/resolve/main/control-LoRAs-rank256/control-lora-depth-rank256.safetensors -P $base_dir/controlnet/
wget -N --show-progress -c https://huggingface.co/stabilityai/control-lora/resolve/main/control-LoRAs-rank256/control-lora-recolor-rank256.safetensors -P $base_dir/controlnet/
wget -N --show-progress -c https://huggingface.co/stabilityai/control-lora/resolve/main/control-LoRAs-rank256/control-lora-sketch-rank256.safetensors -P $base_dir/controlnet/

wget -N --show-progress -c https://huggingface.co/stabilityai/control-lora/resolve/main/control-LoRAs-rank128/control-lora-N --show-progress -canny-rank128.safetensors -P $base_dir/controlnet/
wget -N --show-progress -c https://huggingface.co/stabilityai/control-lora/resolve/main/control-LoRAs-rank128/control-lora-depth-rank128.safetensors -P $base_dir/controlnet/
wget -N --show-progress -c https://huggingface.co/stabilityai/control-lora/resolve/main/control-LoRAs-rank128/control-lora-recolor-rank128.safetensors -P $base_dir/controlnet
wget -N --show-progress -c https://huggingface.co/stabilityai/control-lora/resolve/main/control-LoRAs-rank128/control-lora-sketch-rank128-metadata.safetensors -P $base_dir/controlnet/


# wget -N --show-progress -c https://huggingface.co/thibaud/controlnet-openpose-sdxl-1.0/resolve/main/diffusion_pytorch_model.bin -O $base_dir/controlnet/OpenPoseXL2.bin
# GLIGEN
wget -N --show-progress -c https://huggingface.co/comfyanonymous/GLIGEN_pruned_safetensors/resolve/main/gligen_sd14_textbox_pruned_fp16.safetensors -P $base_dir/gligen/
# ESRGAN upscale model
wget -N --show-progress -c https://github.com/xinntao/Real-ESRGAN/releases/download/v0.1.0/RealESRGAN_x4plus.pth -P $base_dir/upscale_models/
wget -N --show-progress -c https://huggingface.co/sberbank-ai/Real-ESRGAN/resolve/main/RealESRGAN_x2.pth -P $base_dir/upscale_models/
wget -N --show-progress -c https://huggingface.co/sberbank-ai/Real-ESRGAN/resolve/main/RealESRGAN_x4.pth -P $base_dir/upscale_models/

wget -N --show-progress -c https://huggingface.co/mcmonkey/google_t5-v1_1-xxl_encoderonly/resolve/main/t5xxl_fp8_e4m3fn.safetensors -P $base_dir/clip/
wget -N --show-progress -c https://huggingface.co/Comfy-Org/HunyuanVideo_repackaged/resolve/main/split_files/diffusion_models/hunyuan_video_t2v_720p_bf16.safetensors -P $base_dir/diffusion_models/
wget -N --show-progress -c https://huggingface.co/Comfy-Org/HunyuanVideo_repackaged/resolve/main/split_files/text_encoders/clip_l.safetensors -P $base_dir/text_encoders/
wget -N --show-progress -c https://huggingface.co/Comfy-Org/HunyuanVideo_repackaged/resolve/main/split_files/text_encoders/llava_llama3_fp8_scaled.safetensors -P $base_dir/text_encoders/
wget -N --show-progress -c https://huggingface.co/Comfy-Org/HunyuanVideo_repackaged/resolve/main/split_files/vae/hunyuan_video_vae_bf16.safetensors -P $base_dir/vae/
wget -N --show-progress -c https://huggingface.co/Kijai/HunyuanVideo_comfy/resolve/main/hunyuan_video_FastVideo_720_fp8_e4m3fn.safetensors -P $base_dir/diffusion_models/

echo "Done"