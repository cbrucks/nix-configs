{ config, pkgs, ... }:

{
  imports = [
    ./dconf.nix
  ];

  home.packages = (with pkgs.gnome; [
  ]) ++ (with pkgs.gnomeExtensions; [
    appindicator # systray icons
    blur-my-shell # adds blurred 
    clipboard-indicator # clipboard indicator
    media-controls # active media controls

    # dask-to-dock # Mac-like dock
    dash-to-panel # Windows-like dock
    arc-menu # application menu modeled after other operating systems
    gtk4-desktop-icons-ng-ding # allows for desktop icons
  ]);

  # gtk = {
  #   enable = true;

  #   iconTheme = {
  #     name = "Windows 10";
  #     package = pkgs.my-icon-theme;
  #   };

  #   theme = {
  #     name = "";
  #     package = ;
  #   };
  # };
}