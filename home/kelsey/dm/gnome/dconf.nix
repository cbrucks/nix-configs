# Generated via dconf2nix: https://github.com/gvolpe/dconf2nix
# dconf dump / | dconf2nix > dconf.nix

{ lib, ... }:

with lib.hm.gvariant;

{
  dconf.settings = {
    "org/gnome/control-center" = {
      last-panel = "display";
      window-state = mkTuple [ 980 640 ];
    };

    "org/gnome/desktop/app-folders" = {
      folder-children = [ "Utilities" "YaST" "20bc4417-c3fb-4b57-931f-e14ec3025422" "be127024-fa93-40a3-b56e-73f5166a4523" "dda7f75f-4650-4f18-b115-288f9c2e737c" "d0b16f88-34e2-4cb4-9db4-403ffbb7f288" ];
    };

    "org/gnome/desktop/app-folders/folders/20bc4417-c3fb-4b57-931f-e14ec3025422" = {
      apps = [ "librewolf.desktop" "brave-browser.desktop" "thunderbird.desktop" ];
      name = "Internet";
    };

    "org/gnome/desktop/app-folders/folders/Utilities" = {
      apps = [ "gnome-abrt.desktop" "gnome-system-log.desktop" "nm-connection-editor.desktop" "org.gnome.baobab.desktop" "org.gnome.Connections.desktop" "org.gnome.DejaDup.desktop" "org.gnome.Dictionary.desktop" "org.gnome.DiskUtility.desktop" "org.gnome.eog.desktop" "org.gnome.Evince.desktop" "org.gnome.FileRoller.desktop" "org.gnome.fonts.desktop" "org.gnome.seahorse.Application.desktop" "org.gnome.tweaks.desktop" "org.gnome.Usage.desktop" "vinagre.desktop" "xterm.desktop" "cups.desktop" ];
      categories = [ "X-GNOME-Utilities" ];
      name = "X-GNOME-Utilities.directory";
      translate = true;
    };

    "org/gnome/desktop/app-folders/folders/YaST" = {
      categories = [ "X-SuSE-YaST" ];
      name = "suse-yast.directory";
      translate = true;
    };

    "org/gnome/desktop/app-folders/folders/be127024-fa93-40a3-b56e-73f5166a4523" = {
      apps = [ "sol.desktop" "org.gnome.TwentyFortyEight.desktop" "atomix.desktop" "org.gnome.Chess.desktop" "org.gnome.Four-in-a-row.desktop" "org.gnome.Klotski.desktop" "org.gnome.Hitori.desktop" "org.gnome.five-or-more.desktop" "org.gnome.Tali.desktop" "org.gnome.Taquin.desktop" "org.gnome.Sudoku.desktop" "org.gnome.SwellFoop.desktop" "org.gnome.Tetravex.desktop" "org.gnome.Reversi.desktop" "org.gnome.LightsOff.desktop" "org.gnome.Quadrapassel.desktop" "org.gnome.Robots.desktop" "org.gnome.Nibbles.desktop" "org.gnome.Mines.desktop" "org.gnome.Mahjongg.desktop" ];
      name = "Games";
    };

    "org/gnome/desktop/app-folders/folders/d0b16f88-34e2-4cb4-9db4-403ffbb7f288" = {
      apps = [ "vlc.desktop" "com.sayonara-player.Sayonara.desktop" ];
      name = "Sound & Video";
    };

    "org/gnome/desktop/app-folders/folders/dda7f75f-4650-4f18-b115-288f9c2e737c" = {
      apps = [ "draw.desktop" "impress.desktop" "calc.desktop" "base.desktop" "math.desktop" "writer.desktop" ];
      name = "Office";
    };

    "org/gnome/desktop/background" = {
      color-shading-type = "solid";
      picture-options = "zoom";
      picture-uri = "file:///home/kelsey/.local/share/backgrounds/2024-02-04-15-09-53-SydneyOperaHouse.webp";
      picture-uri-dark = "file:///home/kelsey/.local/share/backgrounds/2024-02-04-15-09-53-SydneyOperaHouse.webp";
      primary-color = "#000000000000";
      secondary-color = "#000000000000";
    };

    "org/gnome/desktop/input-sources" = {
      sources = [ (mkTuple [ "xkb" "us" ]) ];
      xkb-options = [ "terminate:ctrl_alt_bksp" ];
    };

    "org/gnome/desktop/interface" = {
      clock-format = "12h";
      clock-show-date = true;
      clock-show-weekday = false;
      color-scheme = "default";
      font-antialiasing = "grayscale";
      font-hinting = "slight";
      icon-theme = "Windows 10";
      show-battery-percentage = true;
    };

    "org/gnome/desktop/notifications" = {
      application-children = [ "org-gnome-extensions" ];
    };

    "org/gnome/desktop/notifications/application/org-gnome-extensions" = {
      application-id = "org.gnome.Extensions.desktop";
    };

    "org/gnome/desktop/peripherals/touchpad" = {
      tap-to-click = true;
      two-finger-scrolling-enabled = true;
    };

    "org/gnome/desktop/privacy" = {
      old-files-age = mkUint32 30;
      recent-files-max-age = -1;
    };

    "org/gnome/desktop/remote-desktop/rdp" = {
      enable = true;
      tls-cert = "/home/kelsey/.local/share/gnome-remote-desktop/rdp-tls.crt";
      tls-key = "/home/kelsey/.local/share/gnome-remote-desktop/rdp-tls.key";
      view-only = false;
    };

    "org/gnome/desktop/screensaver" = {
      color-shading-type = "solid";
      lock-delay = mkUint32 30;
      picture-options = "zoom";
      picture-uri = "file:///home/kelsey/.local/share/backgrounds/2024-02-04-15-09-53-SydneyOperaHouse.webp";
      primary-color = "#000000000000";
      secondary-color = "#000000000000";
    };

    "org/gnome/desktop/search-providers" = {
      sort-order = [ "org.gnome.Contacts.desktop" "org.gnome.Documents.desktop" "org.gnome.Nautilus.desktop" ];
    };

    "org/gnome/desktop/session" = {
      idle-delay = mkUint32 900;
    };

    "org/gnome/desktop/wm/preferences" = {
      button-layout = "appmenu:minimize,maximize,close";
    };

    "org/gnome/evolution-data-server" = {
      migrated = true;
    };

    "org/gnome/hitori" = {
      window-maximized = false;
      window-position = mkTuple [ 26 23 ];
      window-size = mkTuple [ 375 391 ];
    };

    "org/gnome/mutter" = {
      attach-modal-dialogs = true;
      dynamic-workspaces = true;
      edge-tiling = true;
      focus-change-on-pointer-rest = true;
      overlay-key = "Super_L";
      workspaces-only-on-primary = false;
    };

    "org/gnome/nautilus/preferences" = {
      default-folder-viewer = "icon-view";
      migrated-gtk-settings = true;
      search-filter-time-type = "last_modified";
    };

    "org/gnome/nautilus/window-state" = {
      initial-size = mkTuple [ 890 550 ];
    };

    "org/gnome/portal/filechooser/chromium-browser" = {
      last-folder-path = "/home/kelsey";
    };

    "org/gnome/portal/filechooser/gnome-background-panel" = {
      last-folder-path = "/home/kelsey/Pictures";
    };

    "org/gnome/settings-daemon/plugins/power" = {
      power-button-action = "interactive";
      sleep-inactive-ac-timeout = 1800;
      sleep-inactive-battery-timeout = 1200;
    };

    "org/gnome/shell" = {
      app-picker-layout = "[{'org.gnome.Snapshot.desktop': <{'position': <0>}>, 'org.gnome.clocks.desktop': <{'position': <1>}>, 'org.gnome.Loupe.desktop': <{'position': <2>}>, 'org.gnome.Calculator.desktop': <{'position': <3>}>, 'gnome-system-monitor.desktop': <{'position': <4>}>, 'org.gnome.Extensions.desktop': <{'position': <5>}>, 'nixos-manual.desktop': <{'position': <6>}>, 'org.gnome.TextEditor.desktop': <{'position': <7>}>, '20bc4417-c3fb-4b57-931f-e14ec3025422': <{'position': <8>}>, 'be127024-fa93-40a3-b56e-73f5166a4523': <{'position': <9>}>, 'dda7f75f-4650-4f18-b115-288f9c2e737c': <{'position': <10>}>, 'Utilities': <{'position': <11>}>, 'd0b16f88-34e2-4cb4-9db4-403ffbb7f288': <{'position': <12>}>, 'solaar.desktop': <{'position': <13>}>, 'clamtk.desktop': <{'position': <14>}>}]";
      disabled-extensions = [ "window-list@gnome-shell-extensions.gcampax.github.com" "workspace-indicator@gnome-shell-extensions.gcampax.github.com" "places-menu@gnome-shell-extensions.gcampax.github.com" "user-theme@gnome-shell-extensions.gcampax.github.com" "apps-menu@gnome-shell-extensions.gcampax.github.com" ];
      enabled-extensions = [ "dash-to-dock@micxgx.gmail.com" "allowlockedremotedesktop@kamens.us" "dash-to-panel@jderose9.github.com" "appindicatorsupport@rgcjonas.gmail.com" "blur-my-shell@aunetx" "clipboard-indicator@tudmotu.com" "mediacontrols@cliffniff.github.com" "drive-menu@gnome-shell-extensions.gcampax.github.com" "light-style@gnome-shell-extensions.gcampax.github.com" "arcmenu@arcmenu.com" ];
      favorite-apps = [ "org.gnome.Settings.desktop" "org.gnome.Nautilus.desktop" "org.keepassxc.KeePassXC.desktop" "chromium-browser.desktop" "startcenter.desktop" ];
      last-selected-power-profile = "performance";
      welcome-dialog-last-shown-version = "43.2";
    };

    "org/gnome/shell/extensions/arcmenu" = {
      apps-show-extra-details = false;
      arc-menu-icon = 44;
      extra-categories = [ (mkTuple [ 0 true ]) (mkTuple [ 1 true ]) (mkTuple [ 2 false ]) (mkTuple [ 3 false ]) (mkTuple [ 4 true ]) ];
      hide-overview-on-startup = true;
      menu-background-color = "rgba(48,48,49,0.98)";
      menu-border-color = "rgb(60,60,60)";
      menu-button-appearance = "Icon";
      menu-button-bg-color = mkTuple [ true "rgba(242,242,242,0.2)" ];
      menu-button-icon = "Menu_Icon";
      menu-foreground-color = "rgb(223,223,223)";
      menu-item-active-bg-color = "rgb(25,98,163)";
      menu-item-active-fg-color = "rgb(255,255,255)";
      menu-item-hover-bg-color = "rgb(21,83,158)";
      menu-item-hover-fg-color = "rgb(255,255,255)";
      menu-layout = "Enterprise";
      menu-separator-color = "rgba(255,255,255,0.1)";
      multi-monitor = true;
      pinned-app-list = [ "Files" "org.gnome.Nautilus" "org.gnome.Nautilus.desktop" "ArcMenu Settings" "/etc/profiles/per-user/kelsey/share/gnome-shell/extensions/arcmenu@arcmenu.com//icons/arcmenu-logo-symbolic.svg" "gnome-extensions prefs arcmenu@arcmenu.com" ];
      power-options = [ (mkTuple [ 0 true ]) (mkTuple [ 1 true ]) (mkTuple [ 4 true ]) (mkTuple [ 2 true ]) (mkTuple [ 3 true ]) (mkTuple [ 5 false ]) (mkTuple [ 6 false ]) (mkTuple [ 7 false ]) ];
      prefs-visible-page = 0;
      search-entry-border-radius = mkTuple [ true 25 ];
      show-activities-button = false;
      show-hidden-recent-files = false;
    };

    "org/gnome/shell/extensions/blur-my-shell/dash-to-dock" = {
      blur = false;
    };

    "org/gnome/shell/extensions/dash-to-dock" = {
      apply-custom-theme = false;
      background-color = "rgb(154,153,150)";
      background-opacity = 0.81;
      custom-background-color = true;
      custom-theme-shrink = true;
      dash-max-icon-size = 36;
      disable-overview-on-startup = false;
      dock-fixed = true;
      dock-position = "BOTTOM";
      extend-height = true;
      height-fraction = 1.0;
      icon-size-fixed = true;
      max-alpha = 0.8;
      multi-monitor = true;
      preferred-monitor = -2;
      preferred-monitor-by-connector = "eDP-1";
      preview-size-scale = 0.0;
      show-apps-at-top = true;
      show-mounts-network = false;
      show-mounts-only-mounted = true;
      transparency-mode = "FIXED";
    };

    "org/gnome/shell/extensions/dash-to-panel" = {
      appicon-margin = 8;
      appicon-padding = 4;
      available-monitors = [ 0 1 ];
      dot-position = "BOTTOM";
      dot-style-focused = "METRO";
      dot-style-unfocused = "METRO";
      focus-highlight = true;
      focus-highlight-color = "#241f31";
      focus-highlight-dominant = false;
      focus-highlight-opacity = 25;
      hide-overview-on-startup = true;
      hotkeys-overlay-combo = "TEMPORARILY";
      leftbox-padding = -1;
      panel-anchors = ''
        {"0":"MIDDLE"}
      '';
      panel-element-positions = ''
        {"0":[{"element":"showAppsButton","visible":false,"position":"stackedTL"},{"element":"leftBox","visible":true,"position":"stackedTL"},{"element":"activitiesButton","visible":false,"position":"stackedTL"},{"element":"taskbar","visible":true,"position":"stackedTL"},{"element":"centerBox","visible":true,"position":"stackedBR"},{"element":"rightBox","visible":true,"position":"stackedBR"},{"element":"systemMenu","visible":true,"position":"stackedBR"},{"element":"dateMenu","visible":true,"position":"stackedBR"},{"element":"desktopButton","visible":true,"position":"stackedBR"}]}
      '';
      panel-lengths = ''
        {"0":100}
      '';
      panel-sizes = ''
        {"0":48}
      '';
      primary-monitor = 0;
      secondarymenu-contains-showdetails = true;
      show-apps-icon-file = "";
      showdesktop-button-width = 30;
      status-icon-padding = -1;
      stockgs-keep-dash = false;
      stockgs-keep-top-panel = false;
      trans-bg-color = "#deddda";
      trans-panel-opacity = 0.75;
      trans-use-custom-bg = false;
      trans-use-custom-opacity = true;
      tray-padding = -1;
      window-preview-title-position = "TOP";
    };

    "org/gnome/shell/extensions/mediacontrols" = {
      mouse-actions = [ "toggle_play" "toggle_menu" "none" "none" "none" "none" "none" "none" ];
    };

    "org/gnome/shell/world-clocks" = {
      locations = [];
    };

    "org/gnome/software" = {
      first-run = false;
    };

    "org/gnome/tweaks" = {
      show-extensions-notice = false;
    };

    "org/gtk/gtk4/settings/color-chooser" = {
      custom-colors = [ (mkTuple [ 0.9490196108818054 0.9490196108818054 0.9490196108818054 0.20000000298023224 ]) ];
      selected-color = mkTuple [ true 0.9490196108818054 0.9490196108818054 0.9490196108818054 0.20000000298023224 ];
    };

    "org/gtk/gtk4/settings/file-chooser" = {
      date-format = "regular";
      location-mode = "path-bar";
      show-hidden = false;
      show-size-column = true;
      show-type-column = true;
      sidebar-width = 140;
      sort-column = "name";
      sort-directories-first = true;
      sort-order = "ascending";
      type-format = "category";
      view-type = "list";
      window-size = mkTuple [ 859 372 ];
    };

    "org/gtk/settings/file-chooser" = {
      clock-format = "12h";
    };

  };
}
