{ lib, config, pkgs, ... }:

{
  imports = [
    ../flatpak.nix
  ];
  
  home.packages = with pkgs; [
    xclicker
  ];

  services.flatpak = {
    enable = true;
    packages = [
      "com.atlauncher.ATLauncher"
    ];
  };
}