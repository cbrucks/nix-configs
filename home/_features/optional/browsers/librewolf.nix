{ lib, config, pkgs, inputs, outputs, ... }:

{
  programs.librewolf = {
    enable = true;
    
    # https://librewolf.net/docs/settings/
    # settings = {
    #   "webgl.disabled" = false;
    #   "privacy.resistFingerprinting" = false;
    # };
  };
}