{ lib, config, pkgs, inputs, outputs, ... }:

{
  programs.chromium = {
    enable = true;
    package = pkgs.ungoogled-chromium;

    commandLineArgs = [
      "--no-default-browser-check"
      "--allow-outdated-plugins"
      "--disable-logging"
      "--disable-breakpad"
      "--flag-switches-begin"
        # TODO revisit this.  It isn't getting applied correctly
        "--extension-mime-request-handling=always-prompt-for-install"
      "--flag-switches-end"
      "--enable-feature=UseOzonPlatform"
      "--ozone-platform=wayland"
    ];

    extensions = [
      {
        # Chromium Web Store (required to install from the web store in ungoogled-chromium)
        id = "ocaahdebbfolfmndjeplogmgcagdmblk";
        updateUrl = "https://raw.githubusercontent.com/NeverDecaf/chromium-web-store/master/updates.xml";
        version = "1.5.4";
      }
    ];
  };
}