{pkgs, config, lib, inputs, outputs, ...}:

{
  imports = [
  ] ++ (builtins.attrValues outputs.homeManagerModules);

  nixpkgs.config = {
    allowUnfree = true;
    allowUnfreePredicate = (_: true);
  };

  nix = {
    package = lib.mkDefault pkgs.nix;
    settings = {
      experimental-features = [ "nix-command" "flakes" ];
      warn-dirty = false;
    };

    # Run the garbage collector regularly
    gc = {
      automatic = true;
      frequency = "weekly";
      # Keep the last 10 generations
      options = "--delete-older-than 60d";
    };
  };

  # Nicely reload system units when changing configs
  systemd.user.startServices = "sd-switch";

  # Let Home Manager install and manage itself.
  programs = {
    home-manager.enable = true;
    git.enable = true;
  };

  home = {
    homeDirectory = lib.mkDefault "/home/${config.home.username}";
    sessionPath = [ "$HOME/.local/bin" ];
    sessionVariables = {
    };

    # persistence = {
    #   "/persist/home/${config.home.username}" = {
    #     directories = [
    #       "Documents"
    #       "Downloads"
    #       "Pictures"
    #       "Videos"
    #       ".local/bin"
    #       ".local/share/nix" # trusted settings and repl history
    #     ];
    #     allowOther = true;
    #   };
    # };
  };
}