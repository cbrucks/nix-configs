{ config, pkgs, inputs, ... }:

{
  imports = [
    # Manage KDE Plasma config declaritively
    # https://github.com/pjones/plasma-manager
    #
    # Convert current config into nix definition use rc2nix
    #   nix run github:pjones/plasma-manager > ~/nix-configs/home/chris/dm/kde/plasma.nix
    inputs.plasma-manager.homeManagerModules.plasma-manager
    # ./plasma.nix
  ];

  home.packages = with pkgs; [
  ];

  services.kdeconnect = {
    enable = true;
    indicator = true;
  };
}