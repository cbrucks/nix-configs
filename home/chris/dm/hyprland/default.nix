{ config, pkgs, ... }:

{
  imports = [
  ];

  # These are done at the host level and are not required to be repeatd.
  #programs.kitty.enable = true; # required for the default Hyprland config

  # Optional, hint Electron apps to use Wayland:
  # home.sessionVariables.NIXOS_OZONE_WL = "1";

  wayland.windowManager.hyprland = {
    #enable = true; # enable Hyprland

    #plugins = [
    #  inputs.hyprland-plugins.packages.${pkgs.stdenv.hostPlatform.system}.hyprbars
    #  "/absolute/path/to/plugin.so"
    #];

    settings = {

    };
  };

  home.packages = with pkgs; [
  ];
}