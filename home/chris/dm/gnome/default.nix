{ config, pkgs, ... }:

{
  imports = [
    ./dconf.nix
  ];

  home.packages = with pkgs.gnomeExtensions; [
    appindicator # systray icons
    blur-my-shell
    clipboard-indicator
    media-controls
    tailscale-status # tailscale status and control
    #tophat # TODO This one isn't detecting the gtop installed # elegant system resource monitor
    #gamemode # Status indicator for GameMode
    vitals # system utilzation info
  ];
}