# Generated via dconf2nix: https://github.com/gvolpe/dconf2nix
# dconf dump / | dconf2nix > dconf.nix

{ lib, ... }:

with lib.hm.gvariant;

{
  dconf.settings = {

    "org/gnome/desktop/app-folders" = {
      folder-children = [ "Chat" "Games" "Media" "Office" "Utilities" "Web" ];
    };

    "org/gnome/desktop/app-folders/folders/Chat" = {
      apps = [ "discord.desktop" "element-desktop.desktop" "Fluffychat.desktop" "info.mumble.Mumble.desktop" ];
      name = "Chat";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/Games" = {
      apps = [ "com.moonlight_stream.Moonlight.desktop" "sol.desktop" "atomix.desktop" "org.gnome.Chess.desktop" "org.gnome.five-or-more.desktop" "org.gnome.Four-in-a-row.desktop" "org.gnome.Hitori.desktop" "org.gnome.Klotski.desktop" "org.gnome.LightsOff.desktop" "org.gnome.Mahjongg.desktop" "org.gnome.Mines.desktop" "org.gnome.Nibbles.desktop" "org.gnome.Quadrapassel.desktop" "org.gnome.Reversi.desktop" "org.gnome.Robots.desktop" "org.gnome.Sudoku.desktop" "org.gnome.SwellFoop.desktop" "org.gnome.Tali.desktop" "org.gnome.Taquin.desktop" "org.gnome.Tetravex.desktop" "org.gnome.Totem.desktop" "org.gnome.TwentyFortyEight.desktop" ];
      name = "Games";
    };

    "org/gnome/desktop/app-folders/folders/Media" = {
      apps = [ "com.sayonara-player.Sayonara.desktop" "vlc.desktop" "gimp.desktop" ];
      name = "Media";
    };

    "org/gnome/desktop/app-folders/folders/Office" = {
      apps = [ "startcenter.desktop" "base.desktop" "calc.desktop" "draw.desktop" "impress.desktop" "math.desktop" "writer.desktop" ];
      name = "Office";
    };

    "org/gnome/desktop/app-folders/folders/Utilities" = {
      apps = [ "gnome-abrt.desktop" "gnome-system-log.desktop" "nm-connection-editor.desktop" "org.gnome.baobab.desktop" "org.gnome.Connections.desktop" "org.gnome.DejaDup.desktop" "org.gnome.Dictionary.desktop" "org.gnome.DiskUtility.desktop" "org.gnome.Evince.desktop" "org.gnome.FileRoller.desktop" "org.gnome.fonts.desktop" "org.gnome.Loupe.desktop" "org.gnome.seahorse.Application.desktop" "org.gnome.tweaks.desktop" "org.gnome.Usage.desktop" "gnome-system-monitor.desktop" "htop.desktop" "cups.desktop" "nixos-manual.desktop" "vinagre.desktop" "xterm.desktop" "solaar.desktop" "nvtop.desktop" ];
      categories = [ "X-GNOME-Utilities" ];
      name = "Utilities";
      translate = true;
    };

    "org/gnome/desktop/app-folders/folders/Web" = {
      apps = [ "brave-browser.desktop" "chromium-browser.desktop" "librewolf.desktop" "torbrowser.desktop" ];
      name = "Web";
    };

    "org/gnome/desktop/background" = {
      color-shading-type = "solid";
      picture-opacity = 100;
      picture-options = "zoom";
      picture-uri = "file:///home/chris/.config/wallpaper/wallpaper";
      picture-uri-dark = "file:///home/chris/.config/wallpaper/wallpaper";
      primary-color = "#023c88";
      secondary-color = "#5789ca";
    };

    "org/gnome/desktop/input-sources" = {
      sources = [ (mkTuple [ "xkb" "us" ]) ];
      xkb-options = [ "terminate:ctrl_alt_bksp" ];
    };

    "org/gnome/desktop/interface" = {
      clock-format = "12h";
      clock-show-date = true;
      clock-show-seconds = false;
      clock-show-weekday = true;
      color-scheme = "prefer-dark";
      document-font-name = "Cantarell 11";
      font-antialiasing = "grayscale";
      font-hinting = "slight";
      font-name = "Cantarell 11";
      font-rgba-order = "rgb";
      monospace-font-name = "Source Code Pro 10";
      show-battery-percentage = true;
      text-scaling-factor = 1.0;
    };

    "org/gnome/desktop/peripherals/keyboard" = {
      numlock-state = true;
    };

    "org/gnome/desktop/peripherals/touchpad" = {
      tap-to-click = true;
      two-finger-scrolling-enabled = true;
    };

    "org/gnome/desktop/privacy" = {
      old-files-age = mkUint32 30;
      recent-files-max-age = -1;
      remember-recent-files = false;
      remove-old-temp-files = true;
      remove-old-trash-files = true;
    };

    "org/gnome/desktop/remote-desktop/rdp" = {
      enable = true;
    };

    "org/gnome/desktop/screensaver" = {
      color-shading-type = "solid";
      lock-delay = 15;
      lock-enabled = true;
      picture-opacity = 100;
      picture-options = "zoom";
      picture-uri = "file:///home/chris/.config/wallpaper/wallpaper";
      primary-color = "#023c88";
      secondary-color = "#5789ca";
      show-full-name-in-top-bar = true;
      user-switch-enabled = true;
    };

    "org/gnome/desktop/session" = {
      idle-delay = 900;
    };

    "org/gnome/desktop/wm/preferences" = {
      button-layout = "appmenu:minimize,maximize,close";
      titlebar-font = "Cantarell Bold 11";
    };

    "org/gnome/mutter" = {
      draggable-border-width = 15;
      dynamic-workspaces = true;
      edge-tiling = true;
    };

    "org/gnome/nautilus/preferences" = {
      always-use-location-entry = true;
      search-filter-time-type = "last_modified";
      show-image-thumbnails = "always";
    };

    "org/gnome/settings-daemon/plugins/color" = {
      night-light-enabled = false;
    };

    "org/gnome/settings-daemon/plugins/media-keys" = {
      custom-keybindings = [ "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/" ];
      home = [ "<Super>e" ];
    };

    "org/gnome/settings-daemon/plugins/power" = {
      ambient-enabled = true;
      idle-brightness = 30;
      idle-dim = true;
      power-button-action = "suspend";
      power-saver-profile-on-low-battery = true;
      sleep-inactive-ac-timeout = 1800;
      sleep-inactive-ac-type = "suspend";
      sleep-inactive-battery-timeout = 1200;
      sleep-inactive-battery-type = "hibernate";
    };

    "org/gnome/shell" = {
      enabled-extensions = [ "allowlockedremotedesktop@kamens.us" "apps-menu@gnome-shell-extensions.gcampax.github.com" "drive-menu@gnome-shell-extensions.gcampax.github.com" "gsconnect@andyholmes.github.io" "mediacontrols@cliffniff.github.com" "places-menu@gnome-shell-extensions.gcampax.github.com" "tailscale-status@maxgallup.github.com" "tophat@fflewddur.github.io" "Vitals@CoreCoding.com" ];
      favorite-apps = [ "org.gnome.Settings.desktop" "org.gnome.Console.desktop" "org.gnome.Nautilus.desktop" "org.keepassxc.KeePassXC.desktop" "brave-browser.desktop" "librewolf.desktop" "codium.desktop" "org.gnome.Meld.desktop" "thunderbird.desktop" "synthesia.desktop" ];
      last-selected-power-profile = "performance";
    };

    "org/gnome/shell/extensions/vitals" = {
      fixed-widths = true;
      hide-icons = false;
      hot-sensors = [ "_processor_usage_" "_memory_usage_" "__temperature_max__" "_storage_free_" ];
      menu-centered = false;
      position-in-panel = 1;
      show-battery = false;
      show-network = true;
      use-higher-precision = false;
    };

    "org/gtk/gtk4/settings/file-chooser" = {
      show-hidden = true;
    };

    "org/gtk/settings/file-chooser" = {
      show-hidden = true;
    };

  };
}
