{ lib, config, pkgs, inputs, outputs, ... }:

{
  imports = [
    ./common

    ./gui

    ./dm/kde

    #../_features/optional/gaming/emulators.nix
    ../_features/optional/gaming/lutris.nix
    ../_features/optional/gaming/heroic.nix
    ../_features/optional/gaming/minecraft.nix

    ../_features/optional/ai/upscayl.nix
  ];

  home.packages = with pkgs; [
    yt-dlp
    ffmpeg_6-full
  ];

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.
}
