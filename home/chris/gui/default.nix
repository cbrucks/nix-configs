{ lib, config, pkgs, inputs, outputs, ... }:

{
  imports = [
    ../../_features/optional/browsers/librewolf.nix
    ../../_features/optional/browsers/ungoogled-chromium.nix
    
    ./vscodium
  ];

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    ## ========== Web Browsers ==========
    #vivaldi
    brave
    tor-browser-bundle-bin 
    ## ==========     Email    ==========
    thunderbird       # email client
    protonmail-bridge # decrypts proton mail and provides local server to connect other applications to
    ## ==========    Office    ==========
    libreoffice       # office suite
    ## ==========    Chat    ==========
    discord           # Voice and text chat
    mumble            # Low-latency, high-quality voice chat
    element-desktop   # Matrix client
    #fluffychat        # Matrix client
    ## ==========    Media     ==========
    vlc               # Media application
    #strawberry       # Audiophile music application
    #tauon            # music player
    #sayonara         # music player
    elisa             # KDE music player
    gimp              # GNU Image Manipulation Program
    #gimp-with-plugins # GNU Image Manipulation Program (with plugins)
    krita             # Painting Application
    ## ==========  Streaming   ==========
    remmina           # remote desktop client
    delfin            # Native Jellyfin client
    waypipe           # A transparent proxy for Wayland applications (waypipe ssh myuser@linuxbox plasmashell)
    ## ==========     VPN      ==========
    protonvpn-gui
    ## ==========  Accounting  ==========
    kmymoney
    skrooge
    homebank
    gnucash
    ## ==========  Utilities   ==========
    keepassxc         # password manager
    # logseq          # Knowledge management system; currently using flatpak as there is a bug in the version in 23.05
    meld              # File/Folder compare GUI
    #localsend         # Alternative to AirDrop
    blanket           # Sound scapes
    celeste           # File Syncing
    #nextcloud-client  # nextcloud desktop client
    libation          # An Audible audiobook manager
    audible-cli       # A command line interface for audible package. With the cli you can download your Audible books, cover, chapter files
    waycheck          # Wayland protocol support checker
    # MIDI piano trainers
    synthesia
    #neothesia
    #linthesia
  ] ++ ( with inputs.nixpkgs-unstable; [
    #gimp-with-plugins # GNU Image Manipulation Program
  ]);

  services = {
    syncthing = {
      enable = true;
      # Syncthing tray service configuration.
      tray = {
        enable = true;
      };
      # Extra command-line arguments to pass to syncthing.
      # extraOptions = [];
    };

    flatpak = {
      enable = true;
      # By default nix-flatpak will add the flathub remote.
      # NOTE: there is a bug were if the remote setting is defined the default flathub remote is removed.
      # https://github.com/gmodena/nix-flatpak/issues/32
      # remotes = [
      #   {
      #     name = "flathub-beta";
      #     location = "https://flathub.org/beta-repo/flathub-beta.flatpakrepo";
      #   }
      # ];

      packages = [
        "com.logseq.Logseq"
        "com.atlauncher.ATLauncher"
        "com.pokemmo.PokeMMO"
        "com.moonlight_stream.Moonlight"
        "io.github.flattool.Warehouse"
        "com.github.tchx84.Flatseal"
        #"fr.handbrake.ghb"
        #"com.makemkv.MakeMKV"
      ];

      

      update = {
        # enable updates at system activation.
        #onActivation = true;
        auto = {
          enable = true;
          onCalendar = "weekly"; # Default value
        };
      };
    };
  };
}
