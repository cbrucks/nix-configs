{ lib, config, pkgs, inputs, outputs, ... }:

{
  imports = [
    ./extensions.nix
    ./notepadplusplus-keybinds.nix
  ];

  programs.vscode = {
    # Whether to enable Visual Studio Code
    enable = true;

    # Version of Visual Studio Code to install
    package = pkgs.vscodium;

    # Whether to enable update checks/notifications.
    enableUpdateCheck = false;

    # Whether to enable update notifications for extensions.
    enableExtensionUpdateCheck = true;

    # Whether extensions can be installed or updated manually or by Visual Studio Code.
    mutableExtensionsDir = true;

    # Configuration written to Visual Studio Code's tasks.json
    userTasks = [
      #{
      #  type = "shell";
      #  label = "Example task";
      #  command = "ls -la";
      #}
    ];

    # Configuration written to Visual Studio Code's settings.json
    userSettings = {
      "files.autoSave" = "afterDelay";
      "editor.renderWhitespace" = "all";
      "editor.insertSpaces" = true;
      "editor.tabSize" = 2;
      #"workbench.colorTheme" = "Tokyo Night";
      "workbench.iconTheme" = "material-icon-theme";
      "workbench.commandPalette.experimental.suggestCommands" = true;
      "workbench.list.smoothScrolling" = true;
      "diffEditor.ignoreTrimWhitespace" = false;
      "git.confirmSync" = false;
      "git.autoStash" = true;
    };

    # Defines global user snippets
    globalSnippets = {
      fixme = {
        body = [
          "$LINE_COMMENT FIXME: $0"
        ];
        description = "Insert a FIXME remark";
        prefix = [
          "fixme"
        ];
      };
    };

    # Defines user snippets for different languages.
    languageSnippets = {
      haskell = {
        fixme = {
          body = [
            "$LINE_COMMENT FIXME: $0"
          ];
          description = "Insert a FIXME remark";
          prefix = [
            "fixme"
          ];
        };
      };
    };
  };
}