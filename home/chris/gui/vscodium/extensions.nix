{ lib, config, pkgs, inputs, outputs, ... }:

{
  programs.vscode = {
    # The extensions Visual Studio Code should be started with
    extensions = with pkgs.vscode-extensions; [
      bbenoist.nix
      ban.spellright
      esbenp.prettier-vscode
      pkief.material-icon-theme
      yzhang.markdown-all-in-one
      ryu1kn.partial-diff
      ritwickdey.liveserver
      eamodio.gitlens
      gitlab.gitlab-workflow
    ];
    # =====================================================
    # language support
    # =====================================================
    ## Nix
    #bbenoist.nix
    #jnoortheen.nix-ide # Nix language support with formatting and error report. (requires language server)
    #brettm12345.nixfmt-vscode # Nix code formatting based on nixfmt tool
    #b4dm4n.vscode-nixpkgs-fmt # Format nix files with nixpkgs-fmt.
    #arrterian.nix-env-selector
    #kamadorueda.alejandra
    ## CMake
    #twxs.cmake
    #ms-vscode.cmake-tools
    #cmake tools
    ## Lua
    #sumneko.lua
    ## .env
    #mikestead.dotenv
    #dotenv.dotenv-vscode
    #----irongeek.vscode-env
    ## Python
    #ms-python.python
    ## C/C++
    #ms-vscode.cpptools
    #xaver.clang-format
    ## TOML
    #bungcip.better-toml
    ## XML
    #dotjoshjohnson.xml
    #----redhat.vscode-xml
    ## YAML
    #----redhat.vscode-yaml
    ## Rust
    #rust-lang.rust
    #serayuzgur.crates
    ## log files
    #ibm.output-colorizer
    ## shell
    #----timonwong.shellcheck
    ## makefile
    #----ms-vscode.makefile-tools
    ## markdown
    #yzhang.markdown-all-in-one
    ## LATEX, markdown
    #valentjn.vscode-ltex
    ## shellscrip, Dockerfile, properties, gitignore, dotenv, hosts, jvmoptions...
    #----foxundermoon.shell-format
    ## systemd unit files
    #----coolbear.systemd-unit-file
    ## Docker
    #----ms-azuretools.vscode-docker
    ## Caddy file
    #matthewpi.caddyfile-support


    # =====================================================
    # utilities
    # =====================================================
    #ban.spellright
    #esbenp.prettier-vscode
    #spywhere.guides
    #hookyqr.beautify
    #----bierner.docs-view
    #tyriar.sort-lines
    #ryu1kn.partial-diff
    #----usernamehw.errorlens
    #----gruntfuggly.todo-tree
    #oderwat.indent-rainbow
    #_2gua.rainbow-brackets
    ## Website live server
    #ritwickdey.liveserver
    #ms-vscode.live-server

    # =====================================================
    # tools
    # =====================================================
    ## Markdown note systems
    #foam.foam-vscode
    #svsool.markdown-memo
    #----tailscale.vscode-tailscale
    #----ms-vscode-remote.remote-ssh

    # =====================================================
    # git
    # =====================================================
    #eamodio.gitlens
    #mhutchie.git-graph
    #waderyan.gitblame
    #gitlab.gitlab-workflow
    #knisterpeter.vscode-github
    #donjayamanne.githistory

    # =====================================================
    # themes
    # =====================================================
    #enkia.tokyo-night
    #catppuccin.catppuccin-vsc
    #github.github-vscode-theme
    #piousdeer.adwaita-theme

    # =====================================================
    # icons
    # =====================================================
    #file-icons.file-icons
    #vscode-icons-team.vscode-icons
    #pkief.material-icon-theme

    # =====================================================
    # key maps
    # =====================================================
    #ms-vscode.notepadplusplus-keybindings
  };
}