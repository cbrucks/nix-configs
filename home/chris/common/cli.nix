{ lib, config, pkgs, inputs, outputs, ... }:

{
  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    ## ==================================================================
    ## Misc
    ## ==================================================================
    tree          # list contents of directories in a tree-like format.
    wget          # list contents of directories in a tree-like format.
    tmux          # terminal multiplexer
    openssl       # general-purpose cryptography and secure communication tool
    glow          # renders markdown on CLI

    ## ==================================================================
    ## Networking
    ## ==================================================================
    wol           # wake on lan utility
    sshfs         # mount remote filesystem using sftp

    ## ==================================================================
    ## Compression
    ## ==================================================================
    zip           # Compressor/archiver for creating and modifying zipfiles
    unzip         # An extraction utility for archives compressed in .zip format
    xz            # A general-purpose data compression software, successor of LZMA
    p7zip         # A file archiver with a high compression ratio.

    ## ==================================================================
    ## Nix
    ## ==================================================================
    nix-index     # a files database for nixpkgs
    nh            # nix helper

    ## ==================================================================
    ## System
    ## ==================================================================
    fastfetch          # System Info
    pciutils           # ls* tool to query system PCI information
    usbutils           # ls* tool to query system USB information
    lshw               # ls* tool to query system hardware information
    htop               # interactive process viewer
    gtop               # system monitoring dashboard for the terminal
    nvtopPackages.full # A (h)top like task monitor for AMD, Adreno, Intel and NVIDIA GPUs
    nmon               # computer performance system monitor tool
    du-dust            # du + rust = dust. Like du but more intuitive.
    duf                # Disk Usage/Free Utility.
    which              # Shows the full path of (shell) commands
    inotify-tools      # monitor a directory for file changes. Ex: inotifywait -m -r /path/to/monitor
    dmidecode          # Parses SMBIOS (DMI) data (useful for reading serial numbers)
    tldr               # Simplified and community-driven man pages

    ## ==================================================================
    ## Editors
    ## ==================================================================
    neovim        # Vim text editor fork focused on extensibility and agility

    ## ==================================================================
    ## Terminals
    ## ==================================================================
    # kitty         # A modern, hackable, featureful, OpenGL based terminal emulator
    # tabby         # [not in nixpkgs] A terminal for the modern age
    # extraterm     # [not in nixpkgs] The swiss army chainsaw of terminal emulators
  ];
}