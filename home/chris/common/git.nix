{ lib, config, pkgs, inputs, outputs, ... }:

{
  programs.git = {
    enable = true;

    userName = "Chris Brucks";
    userEmail = "3113870+cbrucks@users.noreply.github.com";

    extraConfig = {
      pull.rebase = false;
    };

    # List of defining attributes set globally.
    attributes = [];

    # Whether to enable Git Large File Storage
    lfs.enable = true;

    # Whether to enable the delta syntax highlighter. See https://github.com/dandavison/delta.
    delta.enable = true;
  };

  programs.gitui = {
      enable = true;
  };
}