{ lib, config, pkgs, ... }:

{
  # enable fontconfig configuration.
  # This will, for example, allow fontconfig to discover fonts and configurations installed through home.packages and nix-env.
  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    roboto
    roboto-mono
    source-code-pro
    ibm-plex
    b612
  ];
}