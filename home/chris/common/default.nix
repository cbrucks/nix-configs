{ lib, config, pkgs, inputs, outputs, ... }:

{
  imports = [
    ../../_features/global
    
    ../../_features/optional/flatpak.nix

    ./cli.nix
    ./fonts.nix
    ./git.nix
  ];

  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "chris";

  # environment variables
  home.sessionVariables = {
    # EDITOR = "emacs";
  };
}
