#!/bin/sh

sudo nixos-firewall-tool open tcp 9090

# nix run github:nixified-ai/flake#invokeai-amd
# nix run github:nixified-ai/flake#invokeai-nvidia
nix run github:nixified-ai/flake#invokeai-nvidia -- --host 192.168.78.4

sudo nixos-firewall-tool reset

