#!/bin/sh

ls -la

yt-dlp \
    --output "~/Music/%(uploader)s/%(album)s/%(playlist_index)02d %(track)s" \
    --cookies-from-browser firefox \
    --embed-thumbnail \
    --extract-audio \
    --audio-format m4a \
    --audio-quality 10 \
    --embed-metadata \
    $1
    # --output "~/Music/Various Artists/%(album)s/%(playlist_index)02d %(track)s" \