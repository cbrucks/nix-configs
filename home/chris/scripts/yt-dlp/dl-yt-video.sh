#!/bin/sh

ls -la

yt-dlp \
    --output "~/Videos/%(uploader)s/%(title)s" \
    --cookies-from-browser brave \
    --restrict-filenames \
    --format mp4 \
    $1
    # --embed-thumbnail \
    # --audio-format m4a \
    # --audio-quality 10 \
