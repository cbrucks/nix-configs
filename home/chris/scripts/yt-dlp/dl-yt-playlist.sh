#!/bin/sh

ls -la

yt-dlp \
    --output "~/Music/%(uploader)s/%(album)s/%(track)s" \
    --cookies-from-browser firefox \
    --embed-thumbnail \
    --extract-audio \
    --audio-format m4a \
    --audio-quality 10 \
    --embed-metadata \
    $1