{ lib, config, pkgs, inputs, outputs, ... }:

{
  imports = [
    ../_features/global

    ../_features/optional/browsers/librewolf.nix
    ../_features/optional/browsers/ungoogled-chromium.nix
  ];

  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "hannah";

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  # environment variables
  home.sessionVariables = {
    # EDITOR = "emacs";
  };

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    ## ========== Web Browsers ==========
    brave
    ## ==========     Email    ==========
    thunderbird       # email client
    ## ==========    Office    ==========
    libreoffice       # office suite
    ## ==========    Media     ==========
    vlc               # Media application
    sayonara          # music player
    ## ==========  Utilities   ==========
    keepassxc         # password manager
  ];

  services = {
    syncthing = {
      enable = true;
      # Syncthing tray service configuration.
      tray = {
        enable = true;
      };
      # Extra command-line arguments to pass to syncthing.
      # extraOptions = [];
    };
  };
}
