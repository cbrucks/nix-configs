# https://gitlab.com/-/snippets/2535355
# Authored by: https://gitlab.com/cbrucks (Chris Brucks)

# Usage

# Import module.
# ./input-leap.nix - or wherever  you're going to drop this.

# Add this to your config.
# {
#   services.input-leap = {
#     enable = true;
#     openFirewall = true;
#   };
# }

{ lib, config, pkgs, ... }:
with lib;

let
  cfg = config.services.input-leap;
in
{
  options = {
    services.input-leap = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Whether to install input-leap.
        '';
      };
      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Whether to open the port in the firewall.
        '';
      };
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      # software KVM for sharing mouse, keyboard, and clipboard with other computers
      environment.systemPackages = [
        pkgs.input-leap
      ];
    })
    (mkIf (cfg.enable && cfg.openFirewall) {
      # open the firewall ports
      networking.firewall = {
        allowedTCPPorts = [ 24800 ];
        allowedUDPPorts = [ 24800 ];
      };
    })
  ];
}
