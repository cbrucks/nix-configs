# New Host

- Clone this repo into the admin user's home directory.
- Add a folder under `hosts` with the name of the machine.
- Copy the configuration from `/etc/nixos/configuration.nix` to this directory.
- Symbolically link the configuration file using the fully qualified path back into `/etc/nixos/`.
- Customize the configuration as needed

## Mounting Remote Filesystems

Create a local directory.  
Mount the remote directory.
``` bash
mkdir /home/chris/remote_dir
sshfs [user@]hostname:[directory] /home/chris/mnt/remote_dir
```

Mark the directory as safe for git
``` nix
programs.git.extraConfig = {
  safe.directory = "/home/chris/mnt/remote_dir";
};
```

# Using Flakes
Reference Misterio77's config repo https://github.com/Misterio77/nix-config/

```
$ sudo nixos-rebuild --flake .#hostname build
```

## Cache Errors

```
$ sudo nixos-rebuild --option eval-cache false --flake .#hostname build
```

## Creating a New Flake

```
$ nix flake init --template ./templates/vimjoyer-flake-starter-config
```

# Home Manager

[Home Manager Option Search](https://mipmip.github.io/home-manager-option-search/)

Create a new `home.nix` file.
```
$ nix run home-manager/master -- init
$ cp ~/.config/home-manager/home.nix nix-configs/home-manager/<user>
```

# NixOS Clean Up

```
$ sudo nix-collect-garbage --delete-older-than 15d
```

# User passwords

To add a password to a user, add the following to the user config
``` bash
users.users.<name>.hashedPassword = "<hashed-password>";
```
and generate the hashed password with the following command
``` bash
mkpasswd -m sha-256 'secret_passw*rd'
```

# Interactive Configuration

Interactive exploration of the configuration is possible using `nix repl`, a read-eval-print loop for Nix expressions.
``` bash
nix repl '<nixpkgs/nixos>'
```

# Generations

Every time the system state is rebuilt using nixos-rebuild switch, a new generation is created. You can revert to the previous generation at any time, which is useful if a configuration change (or system update) turns out to be detrimental.

You can roll back via:
``` bash
nix-env --rollback               # roll back a user environment
nixos-rebuild switch --rollback  # roll back a system environment
```

NixOS also places entries for previous generations in the bootloader menu, so as a last resort you can always revert to a previous configuration by rebooting. To set the currently booted generation as the default run
``` bash
/run/current-system/bin/switch-to-configuration boot
```

Because NixOS keeps previous generations of system state available in case rollback is desired, old package versions aren't deleted from your system immediately after an update. You can delete old generations manually:
``` bash
# delete generations older than 30 days
$ nix-collect-garbage --delete-older-than 30d

# delete ALL previous generations - you can no longer rollback after running this
$ nix-collect-garbage -d
```

List generations:
``` bash
sudo nix-env --profile /nix/var/nix/profiles/system --list-generations
```

Switch generations:
``` bash
sudo nix-env --profile /nix/var/nix/profiles/system --switch-generation 204
```

delete broken generation(s):
``` bash
sudo nix-env --profile /nix/var/nix/profiles/system --delete-generations 205 206
```

You can configure automatic garbage collection by setting the [nix.gc](https://search.nixos.org/options?query=nix.gc) options in /etc/nixos/configuration.nix. This is recommended, as it keeps the size of the Nix store down. 

# Command Line access

To drop to the command line from the GUI press `Ctrl`+`Alt`+`F1`

# Resources

- [NixOS Manual](https://nixos.org/manual/nixos/stable/index.html)
- [NixOS Search](https://search.nixos.org)
- [Nix Expression Language Overview](https://nixos.org/manual/nixos/stable/index.html#sec-configuration-syntax)
- [NixOS Overview](https://nixos.wiki/wiki/Overview_of_the_NixOS_Linux_distribution)
- [My NixOS](https://mynixos.com/)
- [Arch - List of Applications](https://wiki.archlinux.org/title/List_of_applications)
